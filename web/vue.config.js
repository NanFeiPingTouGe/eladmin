/**
 * 配置参考: https://cli.vuejs.org/zh/config/
 */
const webpack=require('webpack')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Timestamp = new Date().getTime()

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .test(/\.svg$/)
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
  },
  productionSourceMap: false,
  devServer: {
    open: true,
    port: 8001,
    overlay: {
      errors: true,
      warnings: true
    }
  },
  configureWebpack: {
     plugins: [
        new webpack.ProvidePlugin({
          $:"jquery",
          jQuery:"jquery",
          "windows.jQuery":"jquery"
        }),
        // 修改打包后css文件名
        new MiniCssExtractPlugin({
          filename: `css/[name].${Timestamp}.css`,
          chunkFilename: `css/[name].${Timestamp}.css`
        })
      ],
    output: { // 修改打包后js文件名
      filename: `js/[name].${Timestamp}.js`,
      chunkFilename: `js/[name].${Timestamp}.js`
    }
  }
}
