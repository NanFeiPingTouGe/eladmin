import {addDynamicRoute} from '@/router'

export default {
    data() {
        return {
            processVisible: false
        }
    },
    methods: {
        // 初始化流程配置(集成发起流程和任务处理功能)
        initProcessMultiple(callbacks) {

            console.log("this.$route.params", this.$route.params.processShowType)
            // showType 用于区分流程启动（start）、任务处理(taskHandle)以及查看详情(null)
            // 流程启动显示的是“流程启动”按钮，任务处理显示的是“通过、委托、回退、驳回、终止”按钮，查看详情都不显示
            var showType = this.$route.params.processShowType
            this.processVisible = true
            this.$nextTick(() => {
                if (this.$route.params.processDefinitionKey) {
                    this.$refs.renProcessMultiple.dataForm.processDefinitionKey = this.$route.params.processDefinitionKey
                }
                if (this.$route.params.taskId) {
                    this.$refs.renProcessMultiple.dataForm.taskId = this.$route.params.taskId
                }

                if (this.$route.params.taskName) {
                    this.$refs.renProcessMultiple.dataForm.taskName = this.$route.params.taskName
                }
                if (this.$route.params.processInstanceId) {
                    this.$refs.renProcessMultiple.dataForm.processInstanceId = this.$route.params.processInstanceId
                }
                if (this.$route.params.businessKey) {
                    this.$refs.renProcessMultiple.dataForm.businessKey = this.$route.params.businessKey
                }

                if (showType) {
                    this.$refs.renProcessMultiple.showType = showType
                }

                if (this != undefined) {
                    this.$refs.renProcessMultiple.parentObj = this
                }

                if (callbacks) {
                    this.$refs.renProcessMultiple.callbacks = callbacks
                }


            })
        },
        // 关闭当前窗口
        closeCurrentTab(data) {
            var tabName = this.$store.state.contentTabsActiveName
            this.$store.state.contentTabs = this.$store.state.contentTabs.filter(item => item.name !== tabName)
            if (this.$store.state.contentTabs.length <= 0) {
                this.$store.state.sidebarMenuActiveName = this.$store.state.contentTabsActiveName = 'home'
                return false
            }
            if (tabName === this.$store.state.contentTabsActiveName) {
                this.$router.push({name: this.$store.state.contentTabs[this.$store.state.contentTabs.length - 1].name})
            }
        },
        //跳到我的待办
        jumpToMyTodoTask(){
            this.$router.push("activiti-my-todo-task");
        },
        // 获取流程定义的表单路由配置信息
        getProcDefRouteSet(data, callback) {
            debugger;
            this.$http.get(this.baseUrl.JMPS_ADMIN_URL + `/act/process/getProcDefBizRoute/one`,
                {
                    params: {
                        id: data.processDefinitionId,
                        owner: data.owner
                    }
                }).then(({data: res}) => {
                if (res.code !== 0) {
                    return this.$message.error(res.msg)
                }
                if (!res.data || !res.data.bizRoute) {
                    return this.$message.error(this.$t('process.routeError'))
                }
                var param = {
                    ...data,
                    ...res.data
                }
                callback(param)
            }).catch(() => {
            })
        },
        getProcDefBizRouteAndProcessInstance(params, callback) {
            this.$http.get(this.baseUrl.JMPS_ADMIN_URL + `/act/process/getProcDefBizRouteAndProcessInstance`, {
                params: params
            }).then(({data: res}) => {
                if (res.code !== 0) {
                    return this.$message.error(res.msg)
                }
                if (!res.data || !res.data.bizRoute) {
                    return this.$message.error(this.$t('process.routeError'))
                }
                var param = {
                    ...params,
                    ...res.data
                }
                callback(param)
            }).catch(() => {
            })
        },
        // 根据流程定义KEY获取最新的表单路由配置信息
        getLatestProcDefRouteSet(procDefKey, callback) {
            this.$http.get(this.baseUrl.JMPS_ADMIN_URL + `/act/process/getLatestProcDefBizRoute`, {
                params: {
                    procDefKey: procDefKey
                }
            }).then(({data: res}) => {
                if (res.code !== 0) {
                    return this.$message.error(res.msg)
                }
                if (!res.data || !res.data.bizRoute) {
                    return this.$message.error(this.$t('process.routeError'))
                }
                var param = {
                    procDefKey: procDefKey,
                    ...res.data
                }
                callback(param)
            }).catch(() => {
            })
        },
        // 查看流程图
        forwardDetail(data) {
            var currentTaskName = data.currentTaskList ? data.currentTaskList[0].taskName : ''
            var routeParams = {
                routeName: `${this.$route.name}__detail_${data.processInstanceId}`,
                menuId: `${this.$route.meta.menuId}`,
                title: `${this.$route.meta.title} - ${data.processDefinitionName || data.activityName || currentTaskName}`,
                path: data.bizRoute,
                params: {
                    processInstanceId: data.processInstanceId,
                    businessKey: data.businessKey,
                    businessNum: data.owner,
                    index: 0,
                    processParams: data.processParams
                }

            }
            addDynamicRoute(routeParams, this.$router)
        },
        // 子级 查看流程图
        forwardTaskDetail(data) {
            var routeParams = {
                routeName: `${this.$route.name}__detail_${data.taskId}`,
                menuId: `${this.$route.meta.menuId}`,
                title: `${this.$route.meta.title} - ${data.taskName}`,
                path: data.bizRoute,
                params: {
                    taskId: data.taskId,
                    processInstanceId: data.processInstanceId,
                    businessKey: data.businessKey,
                    businessNum: data.owner,
                    processParams: data.processParams
                }
            }
            addDynamicRoute(routeParams, this.$router)
        },
        forwardHandleUrl(data) {
            console.log(data)
            var routeParams = {
                routeName: `${this.$route.name}__handle_${data.taskId}`,
                menuId: `${this.$route.meta.menuId}`,
                title: `${this.$route.meta.title} - ${data.taskName}`,
                path: data.bizRoute,
                params: {
                    taskId: data.taskId,
                    taskName: data.taskName,
                    processInstanceId: data.processInstanceId,
                    processShowType: 'taskHandle',
                    businessKey: data.businessKey,
                    businessNum: data.owner,
                    processDefinitionKey: data.processDefinitionKey,
                    //八类表单、软硬件评审按钮控制
                    index: 1,
                    processParams: data.processParams
                }

            }
            addDynamicRoute(routeParams, this.$router)
        },
        countersignCall(obj) {
            this.$http["post"](
                this.baseUrl.JMPS_ADMIN_URL + "/act/task/actCompleteByVariables",
                obj
            ).then(({data: res}) => {
                if (res.code !== 0) {
                    return this.$message.error(res.msg);
                }
                this.$message({
                    message: this.$t("prompt.success"),
                    type: "success",
                    duration: 500,
                });
                this.closeCurrentTab()
            });
        },
    }
}
