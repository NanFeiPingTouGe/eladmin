import Cookies from 'js-cookie'
import qs from 'qs'
import { Alert } from 'element-ui';
export default {
    data() {
        /* eslint-disable */
        return {
            // 设置属性
            mixinViewModuleOptions: {
                isPopup: false,//针对弹窗初始化数据设置,弹窗数据用created加载
                activatedIsNeed: true, // 此页面是否在激活（进入）时，调用查询数据列表接口？
                updatedIsNeed: false, // 此页面是否在updated时，更改searchHeight高度
                getDataListURL: '', // 数据列表接口，API地址
                getDataListIsPage: false, // 数据列表接口，是否需要分页？
                deleteURL: '', // 删除接口，API地址
                deleteIsBatch: false, // 删除接口，是否需要批量？
                deleteIsBatchKey: 'id', // 删除接口，批量状态下由那个key进行标记操作？比如：pid，uid...
                exportURL: '' // 导出接口，API地址
            },
            //查询部分高度
            searchHeight: 40,
            // 默认属性
            field: '', // 用户定制表默认所在列   
            dataForm: {}, // 查询条件
            dataList: [], // 数据列表
            showFiled:[],//默认显示的table头的数组
            order: '', // 排序，asc／desc
            orderField: '', // 排序，字段
            page: 1, // 当前页码
            limit: 10, // 每页数
            total: 0, // 总条数
            pageTotalSize:0,//订单条数
            pageTotalSizeCon:0,//合同统计条数
            dataListLoading: false, // 数据列表，loading状态
            dataListSelections: [], // 数据列表，多选项
            addOrUpdateVisible: false // 新增／更新，弹窗visible状态
        }
        /* eslint-enable */
    },
    created() {
        this.getInfo();
        if(this.mixinViewModuleOptions.isPopup){
            this.getDataList()
        }
    },
    activated() {
        this.getInfo();
        if (this.mixinViewModuleOptions.activatedIsNeed) {
            this.getDataList()

        }
    },
    updated(){
        if (this.mixinViewModuleOptions.updatedIsNeed) {
            document.body.style.overflow = 'hidden';
            let that = this;
            //由于updated时，dom结点尚未更新完毕，所以加setTimeout打时间差
            setTimeout(function(){
              if(that.show3 == true){
                that.searchHeight = that.$el.querySelector(".findcss_main") ? that.$el.querySelector(".findcss_main").clientHeight : 0;
              }else{
                //dom生成后，取searchHeight，为计算属性elTableHeight服务
                that.searchHeight = 40;
              }
              
              document.body.style.overflow = '';
            }, 150)
        }
    },
    methods: {
        /**
         * others:根据不同功能，自行减去的固定高度，不带按钮，一般是230，带按钮一般是310
         */
        getTableHeight(others){
            if(others == null){
                others = 310
            }
            //动态根据screenHeight和searchHeight计算表格高度，这个高度也可以根据具体功能，用screenHeight自行计算
            let height = this.$store.state.screenHeight - this.searchHeight - others;
            //高度最小200px
            return height > 200 ? (height + "px") : "200px";
        } ,
        // 获取数据列表
        //paramsStoreFlag：paramsStore的key
        getDataList(flag) {
            if (flag == 1) { //查询时，page置为1
                this.page = 1;
            }
            this.dataListLoading = true
            this.$http.get(
                this.mixinViewModuleOptions.getDataListURL, {
                    params: {
                        order: this.order,
                        orderField: this.orderField,
                        page: this.mixinViewModuleOptions.getDataListIsPage ? this.page : null,
                        limit: this.mixinViewModuleOptions.getDataListIsPage ? this.limit : null,
                        ...this.dataForm
                    }
                }
            ).then(({ data: res }) => {
                this.dataListLoading = false
                if (res.code !== 0) {
                    this.dataList = []
                    this.total = 0
                    return this.$message.error(res.msg)
                }
                this.dataList = this.mixinViewModuleOptions.getDataListIsPage ? res.data.list : res.data
                console.log(this.dataList)
                    //console.log("dataList:", this.dataList)
                this.total = this.mixinViewModuleOptions.getDataListIsPage ? res.data.total : 0
                if(this.dataList.length>0){
                    this.pageTotalSize = 0;
                    this.pageTotalSizeCon = 0;
                    for(var i=0;i<this.dataList.length;i++){
                        //console.log("dataList:", this.dataList[i].children.length)
                        this.pageTotalSize += this.dataList[i].children.length
                        if(this.dataList[i].pid!=null){
                            this.pageTotalSize += 1;
                        }else{
                            if(this.dataForm.conStatusOfSelect){
                                //去除制作中 合同主状态为 提交的
                                if(this.dataForm.conStatusOfSelect=='0'&&this.dataList[i].status!=1&&this.dataList[i].status!=2){
                                    this.pageTotalSizeCon +=1;
                                }
                                if(this.dataForm.conStatusOfSelect=='1'){
                                    this.pageTotalSizeCon +=1;
                                }
                            }else{
                                //去除 废除状态的合同统计
                                if(this.dataList[i].status!=2){
                                    this.pageTotalSizeCon +=1;
                                }
                                //this.pageTotalSizeCon +=1;
                            }
                        }
                    }
                }
            }).catch(() => {
                this.dataListLoading = false
            })
        },
        // 多选
        dataListSelectionChangeHandle(val) {
            this.dataListSelections = val
        },
        // 排序
        dataListSortChangeHandle(data) {
            if (!data.order || !data.prop) {
                this.order = ''
                this.orderField = ''
                return false
            }
            this.order = data.order.replace(/ending$/, '')
            this.orderField = data.prop.replace(/([A-Z])/g, '_$1').toLowerCase()
            this.getDataList()
        },
        // 分页, 每页条数
        pageSizeChangeHandle(val) {
            this.page = 1
            this.limit = val
            this.getDataList()
        },
        // 分页, 当前页
        pageCurrentChangeHandle(val) {
            this.page = val
            this.getDataList()
        },
        // 新增 / 修改
        addOrUpdateHandle(id) {
            console.log(id)
            this.addOrUpdateVisible = true
            this.$nextTick(() => {
                this.$refs.addOrUpdate.dataForm.id = id
                this.$refs.addOrUpdate.init()
            })
        },
        // 删除
        deleteHandle(id) {
            if (this.mixinViewModuleOptions.deleteIsBatch && !id && this.dataListSelections.length <= 0) {
                return this.$message({
                    message: this.$t('prompt.deleteBatch'),
                    type: 'warning',
                    duration: 1500
                })
            }
            this.$confirm(this.$t('prompt.info', { 'handle': this.$t('delete') }), this.$t('prompt.title'), {
                confirmButtonText: this.$t('confirm'),
                cancelButtonText: this.$t('cancel'),
                type: 'warning'
            }).then(() => {
                this.$http.delete(
                    `${this.mixinViewModuleOptions.deleteURL}${this.mixinViewModuleOptions.deleteIsBatch ? '' : '/' + id}`,
                    this.mixinViewModuleOptions.deleteIsBatch ? {
                        'data': id ? [id] : this.dataListSelections.map(item => item[this.mixinViewModuleOptions.deleteIsBatchKey])
                    } : {}
                ).then(({ data: res }) => {
                    if (res.code !== 0) {
                        return this.$message.error(res.msg)
                    }
                    this.$message({
                        message: this.$t('prompt.success'),
                        type: 'success',
                        duration: 1500,
                        onClose: () => {
                            this.getDataList()
                        }
                    })
                }).catch(() => {})
            }).catch(() => {})
        },
        // 导出
        exportHandle() {
            var params = qs.stringify({
                'token': Cookies.get('token'),
                'userid': Cookies.get('userid'),
                ...this.dataForm
            })
            window.location.href = `${window.SITE_CONFIG['apiURL']}${this.mixinViewModuleOptions.exportURL}?${params}`
        },
        // 过滤字段 显示与隐藏 start
        isAuthTableName(key, val) {
            if (val == key) {
                this.field = val;
                return true;
            }
            if (this.showFiled == null || this.showFiled == '') {
                return true;
            }
            return this.showFiled.indexOf(key) !== -1 || false
        },
        filterHandler(filters) {
            var arr = filters[this.field];
            var pageCode = this.mixinViewModuleOptions.pageCode;
            if (arr != null && 　arr.length > 0) {
                this.showFiled = arr;
                this.$http['post'](this.baseUrl.JMPS_ADMIN_URL + '/sys/syspagesetup', {
                    'permissionsStr': arr.join(","),
                    'pageCode': pageCode
                }).then(({ data: res }) => {
                    debugger
                    console.log(res);
                }).catch((data) => {
                    console.log(data);

                })

            }
        },
        //20210219 更新表格头显示值
        updateshowFiled(filters) {
            var arr = filters;
            var pageCode = this.mixinViewModuleOptions.pageCode;
            if (arr != null && 　arr.length > 0) {
                this.showFiled = arr;
                this.$http['post'](this.baseUrl.JMPS_ADMIN_URL + '/sys/syspagesetup', {
                    'permissionsStr': arr.join(","),
                    'pageCode': pageCode
                }).then(({ data: res }) => {
                    console.log(res);
                }).catch((data) => {
                    console.log(data);
                })
            }
        },
        // 获取信息
        getInfo() {
            var that = this;
            this.$http.get(this.baseUrl.JMPS_ADMIN_URL + '/sys/syspagesetup/info', {
                params: {
                    pageCode: this.mixinViewModuleOptions.pageCode
                }
            }).then(({ data: res }) => {
                this.showFiled = res.data
            }).catch(() => {});

        },
        //删除公共func()
        deleteCommon(url, id) {
            this.$http.delete(url, {data: [id]}).then(({data: res}) => {
                if (res.code !== 0) {
                    return this.$message.error(res.msg);
                }
                this.$message({
                    message: this.$t("prompt.success"),
                    type: "success",
                    duration: 500,
                    onClose: () => {
                        this.getDataList();
                    }
                });
            }).catch(() => {

            });
        }
        // end
    }
}