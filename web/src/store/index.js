import Vue from 'vue'
import Vuex from 'vuex'
import cloneDeep from 'lodash/cloneDeep'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
    namespaced: true,
    state: {
        // 导航条, 布局风格, defalut(白色) / colorful(鲜艳)
        navbarLayoutType: 'colorful',
        // 侧边栏, 布局皮肤, default(白色) / dark(黑色)
        sidebarLayoutSkin: 'default',
        // 侧边栏, 折叠状态
        sidebarFold: false,
        // 侧边栏, 菜单
        sidebarMenuList: [],
        sidebarMenuActiveName: '',
        // 内容, 是否需要刷新
        contentIsNeedRefresh: false,
        // 内容, 标签页(默认添加首页)
        contentTabs: [{
            ...window.SITE_CONFIG['contentTabDefault'],
            'name': 'home',
            'title': 'home'
        }],
        contentTabsActiveName: 'home',
        //面包屑
        breadcrumbs: [],
        //右侧main-content的高度，监测浏览器窗口变化resize用
        screenHeight: 800,
        //通知数量
        noticeTotal: 0,

        //要存储在全局中的信息，为key：value格式
        //key: 路由name，不可重复
        //value:路由对应功能需要存储的信息
        paramsStore:[],
        //待办任务统计是否显示
        todoShow: 0,
        //待办统计div中table的数据
        todoStatisticsTableList:[],
        // 内容页 侧边栏, 折叠状态
        projectLeft: false,
        //布局页面跳转url
        projectRouterName:'',
        projectParams:'',
        projectBNList: '',
        projectMenuActiveName: '',//默认选中
        projectMenu:[],
    },
    modules: {
        user
    },
    mutations: {
        // 重置vuex本地储存状态
        resetStore(state) {
            Object.keys(state).forEach((key) => {
                state[key] = cloneDeep(window.SITE_CONFIG['storeState'][key])
            })
        },
        //只管添加和更新，移除在trimBreadcrubs里做
        reduceBreadcrumbs: (state, newObj) => {
            let flag = true;
            for(let i = 0; i < state.breadcrumbs.length; i ++){
                if(state.breadcrumbs[i].rname == newObj.rname){
                    //已存在，更新
                    state.breadcrumbs[i] = cloneDeep(newObj);
                    flag = false;
                    break;
                }
            }
            //不存在，加入
            if(flag){
                state.breadcrumbs.push(newObj);
            }
        },
        //监听contentTabs变化，移除breadcrumbs
        trimBreadcrumbs(state){
            //和contentTabs做对比，tab不存在了，就从breadcrumbs中删除
            state.breadcrumbs.forEach(bread => {
                bread.remove = false;
                let tab = state.contentTabs.filter(item => {
                    return item.name === bread.rname || item.pname === bread.rname;
                  });

                if(tab == null || tab.length == 0){
                    bread.remove = true;
                }
            });

            state.breadcrumbs = state.breadcrumbs.filter(item => item.remove == false);
        },
        resizeMainHeight(state, height){
            if(typeof(height) == "number"){
                state.mainHeight = height;
            }
        },
        /**
         * 更改noticaTotal
         * @param {*} state
         * @param {*} num notice的总数
         */
        updateNoticeTotal(state, num){
            if(typeof(num) == "number"){
                state.noticeTotal = num;
            }
        },
        updateTodoList(state, list){
            state.todoStatisticsTableList = list;
        },
        updateParamsStore(state, newArr){
            state.paramsStore = newArr;
        }
    },
    //根据
    getters: {
        getBreadcrumbByName: (state) => (name) => {
            for(let i = 0; i < state.breadcrumbs.length; i ++){
                let item = state.breadcrumbs[i].list.find((item) => {
                    return item.rname == name;
                });
                if(item){
                    return item;
                }
            }
            return null;
        }
    }
})
