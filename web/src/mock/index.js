import Mock from 'mockjs';

// 配置拦截 ajax 的请求时的行为，支持的配置项目有 timeout。
 Mock.setup({
    timeout: '200 - 400'
})

const dataList = [
    {
        id: "0",
        meetingTime: "2019-12-12 15:22:33",
        meetingName: "采购委员会"
    },
    {
        id: "1",
        meetingTime: "2019-12-15 15:10:03",
        meetingName: "Test1"
    }
];
function queryAvbTopics () {
    return [];
}
function queryMeetingTypes () {
    return {
        data:[
            { id: 0, code: 1, name: "商务例会", attenderList:[{"staffId":"10ef3132-8912-4dd6-a75e-4596d462e37b","name":"王钢铁","phone":"13512313212","email":"wanggangtie@faw.jiefang.com","weichatId":"12345","type":0}] },
            { id: 1, code: 2, name: "采购委员会", attenderList:[{"staffId":"12eb6a84-1bf0-4964-8072-734d9c2330a0","name":"李铁柱","phone":"13512313232","email":"litiezhu@faw.jiefang.com","weichatId":"12345","type":1}] },
        ]
    };
}
function showBasicInfo (params) {
    return {"code":0,"msg":"操作成功","data":{
        "recordList": [],
        "topicApplierId": "null",
        "topicMaxDuration": 0,
        "supList": [],
        "topicType": 0,
        "total": "22.0",
        "methodType": "最低价法",
        "payType": "设备28付款计划",
        "impDescribe": "员工餐ksn no ",
        "planName": "经查，以上供应商不在一汽黑名单之列（一家）。   5\r\n            上述供应商经天眼查查询无关联方共同参与此项目，且不在一汽黑名单范围内（多家）",
        "impDurationDescribe": "dddddd",
        "durationType": "项目周期",
        "impPriceCeiling": "22",
        "reqcode": "SQ2019120060",
        "impPayDescribe": "对对对",
        "purchaseType": "谈判采购",
        "topicId": "1111",
        "meetingCode": "ad7461c6-8a8d-4c96-b6ae-7deeaf4d6ea3",
        "topicName": "员工餐ksn no ",
        "fileList": [{
            "extension": ".xlsx",
            "attachementName": "供应商接口字段（最新）",
            "attachmentType": 0,
            "attachementId": "1212683285801189377",
            "attachementURL": "E:\\JMPS\\JepsUpload\\"
        }, {
            "extension": ".xlsx",
            "attachementName": "接口目录",
            "attachmentType": 0,
            "attachementId": "1212687316938375169",
            "attachementURL": "E:\\JMPS\\JepsUpload\\"
        }, {
            "extension": ".xmind",
            "attachementName": "材料组",
            "attachmentType": 0,
            "attachementId": "1212695026614386690",
            "attachementURL": "E:\\JMPS\\JepsUpload\\"
        }],
        "topicState": "0"
    }}
}

function showApproval(){
    return {"code":0,"msg":"操作成功","data":[{
        "file": {
            "extension": ".vue",
            "attachementName": "login",
            "attachmentType": 0,
            "attachementId": "1210019604869439490",
            "attachementURL": "/opt/JepsUpload/"
        },
        "reqName": "reqpurchasingapply22",
        "background": "reqpurchasingapply",
        "id": "1210019653212975105",
        "source": "0",
        "reqcode": "SQ2019120060"
    }]}
}
function getMetFild(){
    return {"code":0,"msg":"操作成功","data":[{"id":"1201798170116448258","sysCode":"JEPS","entityType":"met_applyfor","entityId":"1201797908198936578","datatype":"1","typeName":"营业执照","alias":"6d55c606-4fc6-4e4c-927c-8ba2be3e307f","extension":".docx","oldName":"文件test","oldUrl":"/opt/JepsUpload/","newName":null,"newUrl":null,"fileCode":null,"creator":1177185315168911361,"createDate":"2019-12-03 17:39:46","updater":null,"updateDate":"2019-12-03 17:39:46","supId":null,"supName":null,"supcode":null,"metId":null,"oldFileName":null,"newFileName":null,"entityIds":null,"flag":null},{"id":"1201798170116448258","sysCode":"JEPS","entityType":"met_applyfor","entityId":"1201797908198936578","datatype":"1","typeName":"营业执照","alias":"6d55c606-4fc6-4e4c-927c-8ba2be3e307f","extension":".docx","oldName":"文件名字一定要够长才可以看到效果","oldUrl":"/opt/JepsUpload/","newName":null,"newUrl":null,"fileCode":null,"creator":1177185315168911361,"createDate":"2019-12-03 17:39:46","updater":null,"updateDate":"2019-12-03 17:39:46","supId":null,"supName":null,"supcode":null,"metId":null,"oldFileName":null,"newFileName":null,"entityIds":null,"flag":null},{"id":"1201798170116448258","sysCode":"JEPS","entityType":"met_applyfor","entityId":"1201797908198936578","datatype":"1","typeName":"营业执照","alias":"6d55c606-4fc6-4e4c-927c-8ba2be3e307f","extension":".docx","oldName":"mysql函数大全23","oldUrl":"/opt/JepsUpload/","newName":null,"newUrl":null,"fileCode":null,"creator":1177185315168911361,"createDate":"2019-12-03 17:39:46","updater":null,"updateDate":"2019-12-03 17:39:46","supId":null,"supName":null,"supcode":null,"metId":null,"oldFileName":null,"newFileName":null,"entityIds":null,"flag":null}]}
}
function findAttachment(){
    return {"code":0,"msg":"操作成功","data":[{"id":"1201798170116448258","sysCode":"JEPS","entityType":"met_applyfor","entityId":"1201797908198936578","datatype":"1","typeName":"营业执照","alias":"6d55c606-4fc6-4e4c-927c-8ba2be3e307f","extension":".docx","oldName":"文件test","oldUrl":"/opt/JepsUpload/","newName":null,"newUrl":null,"fileCode":null,"creator":1177185315168911361,"createDate":"2019-12-03 17:39:46","updater":null,"updateDate":"2019-12-03 17:39:46","supId":"001","supName":"供应商001","supcode":"SUP20191221","metId":null,"oldFileName":null,"newFileName":null,"entityIds":null,"flag":"1"}, {"id":"1201798170116448258","sysCode":"JEPS","entityType":"met_applyfor","entityId":"1201797908198936578","datatype":"1","typeName":"营业执照","alias":"6d55c606-4fc6-4e4c-927c-8ba2be3e307f","extension":".docx","oldName":"文件test","oldUrl":"/opt/JepsUpload/","newName":null,"newUrl":null,"fileCode":null,"creator":1177185315168911361,"createDate":"2019-12-03 17:39:46","updater":null,"updateDate":"2019-12-03 17:39:46","supId":"001","supName":"供应商002","supcode":"SUP20191221","metId":null,"oldFileName":null,"newFileName":null,"entityIds":null,"flag":"1"}, {"id":"1201798170116448258","sysCode":"JEPS","entityType":"met_applyfor","entityId":"1201797908198936578","datatype":"1","typeName":"营业执照","alias":"6d55c606-4fc6-4e4c-927c-8ba2be3e307f","extension":".docx","oldName":"文件test","oldUrl":"/opt/JepsUpload/","newName":null,"newUrl":null,"fileCode":null,"creator":1177185315168911361,"createDate":"2019-12-03 17:39:46","updater":null,"updateDate":"2019-12-03 17:39:46","supId":"001","supName":"供应商003","supcode":"SUP20191221","metId":null,"oldFileName":null,"newFileName":null,"entityIds":null,"flag":"1"}]}
}
function showResult(){
    return {"code":0,"msg":"操作成功","data":{"metApplyforDTO":{"id":"1201812046266585089","entityType":"1","typeName":"0","entityId":"1201773054945669121","sort":11,"metInformationId":"1198801679756087297","panningTime":"1","stutas":"1","remarks":"1","applyforCode":"MET201900002","applyforName":"硬件采购","creator":"1177185315168911361","createDate":"2019-12-03 18:34","creatorName":null,"updater":"1177185314804006913","updateDate":"2019-12-03 18:41","startTime":null,"stutasName":null,"bbServiceSupNum":null,"bbAssistSupNum":null,"qqServiceSupNum":null,"qqAssistSupNum":null,"cdServiceSupNum":null,"cdAssistSupNum":null,"throughTime":null,"fileIds":null,"ids":null,"metCode":null,"metAdds":null,"planningStartTime":null,"metName":null,"metType":null,"reasonApplication":null,"reasonApplicationId":null,"nodeName":null,"formId":null,"documentId":null,"changeSortType":null},"impResultDTO":{"id":"1201773054945669121","planId":"1201749695584641026","code":"JG2019120015","resultStatus":"否","status":"10","creator":"1177185315168911361","createDate":"2019-12-03 15:59:58","updater":"1177185314804006913","updateDate":"2019-12-03 18:41:35","activiDocumentId":null,"impProjectEntity":null,"impResultSupofferList":[{"id":"1201773055037943809","code":null,"supName":"机构20(测试)(cesshi)(递交文件)","total":0.0,"price":0.0,"isSup":"否","originalCode":"1194559577459363841","isAgent":"是","agentBrand":null,"rateOfTaxation":"6%","isWin":null,"relationId":"1201773054945669121","contact":null,"socialCreditCode":"91150925787908439E","creator":"1177185315168911361","createDate":"2019-12-03 15:59:58","updater":null,"updatedate":null,"supperson":"13","supphonenumber":"13844947052","supemail":"765283331@qq.com","suptime":"2019-12-03 15:11","supm":null,"palace":null,"time":"","personpaiming":"1","issupplier":"是","budgetprice":"0.000000","nowsup":null,"nowsupcode":null,"resultTime":null,"metId":null,"metName":null},{"id":"1201773055079886849","code":null,"supName":"机构33","total":0.0,"price":0.0,"isSup":"否","originalCode":"1194559577459363841","isAgent":"是","agentBrand":null,"rateOfTaxation":"6%","isWin":null,"relationId":"1201773054945669121","contact":null,"socialCreditCode":"121212121212121212","creator":"1177185315168911361","createDate":"2019-12-03 15:59:58","updater":null,"updatedate":null,"supperson":"杨永胜","supphonenumber":"13245678111","supemail":"qq@qq.com","suptime":"2019-12-03 14:58","supm":null,"palace":null,"time":"","personpaiming":"2","issupplier":"是","budgetprice":"1.000000","nowsup":null,"nowsupcode":null,"resultTime":null,"metId":null,"metName":null},{"id":"1201773055126024194","code":null,"supName":"测试机构十七","total":0.0,"price":0.0,"isSup":"否","originalCode":null,"isAgent":"是","agentBrand":null,"rateOfTaxation":"6%","isWin":null,"relationId":"1201773054945669121","contact":null,"socialCreditCode":"91140700317148310U","creator":"1177185315168911361","createDate":"2019-12-03 15:59:58","updater":null,"updatedate":null,"supperson":"张三","supphonenumber":"13546431935","supemail":"1053659762@qq.com","suptime":"2019-12-03 15:02","supm":null,"palace":null,"time":"","personpaiming":"3","issupplier":"否","budgetprice":"1.000000","nowsup":null,"nowsupcode":null,"resultTime":null,"metId":null,"metName":null}],"impProjectDTO":{"id":"1201749695584641026","impCode":"FA2019120024","impDescribe":"硬件采购","impType":"5","impMethod":"20","impPayType":"1","activiDocumentId":"20405","impPayDescribe":"测试工作流","impPriceCeiling":"27","impDurationDescribe":"111","impDurationType":"0","status":"7","statusId":null,"creator":1177185315168911361,"createDate":"2019-12-03 06:27:09","updater":1177185315168911361,"updateDate":"2019-12-04 14:46:57","supList":null,"impTime":"2019-12-03","purchaseType":"公开招标","methodType":"最低价法","payType":"设备28付款计划","projectStatus":"已上传","durationType":"项目周期","total":null,"purchasingCode":null,"planName":"公开招标过程符合国家法律规定，结果真实公正。","metId":null,"metName":null,"israte":"是","isRatetype":null}}}}
}
function showConchange(){
    return {"code":0,"msg":"操作成功","data":{"metApplyforDTO":{"id":"1202476222752333826","entityType":"2","typeName":"1","entityId":"1202476146390835201","sort":11,"metInformationId":"1201136447277625346","panningTime":"11","stutas":"0","remarks":"11","applyforCode":"MET201900003","applyforName":"测试开口合同订单","creator":"1177185314959196162","createDate":"2019-12-05 14:34","creatorName":null,"updater":"1177185314707537922","updateDate":"2019-12-05 14:35","startTime":null,"stutasName":null,"bbServiceSupNum":null,"bbAssistSupNum":null,"qqServiceSupNum":null,"qqAssistSupNum":null,"cdServiceSupNum":null,"cdAssistSupNum":null,"throughTime":null,"fileIds":null,"ids":null,"metCode":null,"metAdds":null,"planningStartTime":null,"metName":null,"metType":null,"reasonApplication":null,"reasonApplicationId":null,"nodeName":null,"formId":null,"documentId":null,"changeSortType":null}, "conProcontractDTO": {projectBackground:"projectBackground", updateReason:"updateReason", updataContent:"updataContent"}, "ImpResultSupofferDTO": {resultTime: "2019-12-03 17:39:46", metName:"metName", supName:"供应商002", budgetprice:"1.0", rateOfTaxation:"20%"}}}
}
function findFrame(params){
    return {"code":0,"msg":"操作成功","data":{"ImpFrameProjectBaseDTOList":[{"id":"1208295691500240898","impFrameId":null,"basId":null,"remarks":"33","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":"1177185315168911400","buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"长春市机械物资销售处","supCode":"BA077","impCode":null,"impYear":null,"materialCode":null,"materialName":"11111","score":"3","reason":"rt","identification":null},{"id":"1208295690745266177","impFrameId":null,"basId":null,"remarks":"234234","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":"1177185315168911400","buyerName":null,"status":"1","baseName":"长春","basName":null,"supName":"长春市机械物资销售处","supCode":"BA077","impCode":null,"impYear":null,"materialCode":null,"materialName":"11111","score":"23423","reason":"f","identification":null},{"id":"1208295691089199105","impFrameId":null,"basId":null,"remarks":"234","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"123123","supCode":"CGB1000","impCode":null,"impYear":null,"materialCode":null,"materialName":"磨具磨料及磨具磨料附件","score":"23","reason":"","identification":null},{"id":"1208295691638652929","impFrameId":null,"basId":null,"remarks":"3","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"1219002","supCode":"2019120006","impCode":null,"impYear":null,"materialCode":null,"materialName":"111","score":"王尔德上","reason":"3","identification":null},{"id":"1208295690812375041","impFrameId":null,"basId":null,"remarks":"234","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":"1177185314753675300","buyerName":null,"status":"1","baseName":"长春","basName":null,"supName":null,"supCode":null,"impCode":null,"impYear":null,"materialCode":null,"materialName":"111","score":"234","reason":"f","identification":null},{"id":"1208295691227611137","impFrameId":null,"basId":null,"remarks":"342","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":"1177185315168911400","buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"长春市蓝锐自控科技发展有限公司","supCode":"BA069","impCode":null,"impYear":null,"materialCode":null,"materialName":"磨具磨料及磨具磨料附件","score":"33","reason":"3","identification":null},{"id":"1208295690883678210","impFrameId":null,"basId":null,"remarks":"23","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"1","baseName":"长春","basName":null,"supName":"1219002","supCode":"2019120006","impCode":null,"impYear":null,"materialCode":null,"materialName":"111","score":"234","reason":"f","identification":null},{"id":"1208295691294720002","impFrameId":null,"basId":null,"remarks":"33","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":"1177185315168911400","buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"长春市蓝锐自控科技发展有限公司(取消）","supCode":"CGB006","impCode":null,"impYear":null,"materialCode":null,"materialName":"磨具磨料及磨具磨料附件","score":"3","reason":"324","identification":null},{"id":"1208295690950787073","impFrameId":null,"basId":null,"remarks":"324","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"2","baseName":"成都","basName":null,"supName":null,"supCode":null,"impCode":null,"impYear":null,"materialCode":null,"materialName":"铣齿刀具","score":"324","reason":"fdgd","identification":null},{"id":"1208295691433132034","impFrameId":null,"basId":null,"remarks":"3","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"李志明测试","supCode":"2019120006","impCode":null,"impYear":null,"materialCode":null,"materialName":"11111","score":"33","reason":"3","identification":null},{"id":"1208295690669768705","impFrameId":null,"basId":null,"remarks":"234","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"2","baseName":"长春","basName":null,"supName":"李志明测试","supCode":"2019120006","impCode":null,"impYear":null,"materialCode":null,"materialName":"11111","score":"243","reason":"dfgdf","identification":null},{"id":"1208295691022090241","impFrameId":null,"basId":null,"remarks":"23","base":null,"num":null,"creator":null,"createDate":null,"updater":null,"updateDate":null,"supId":null,"buyerId":null,"buyerName":null,"status":"1","baseName":"成都","basName":null,"supName":"1210ddw","supCode":"2019120006","impCode":null,"impYear":null,"materialCode":null,"materialName":"刀具涂层","score":"324","reason":"ff","identification":null}],"findSummary":[{"materialName":"工装采购","num":4,"impYear":"2019","baseName":"长春"},{"materialName":"工装采购","num":8,"impYear":"2019","baseName":"成都"}],"metApplyforDTO":{"id":"1208295951752531970","entityType":"5","typeName":"0","entityId":"1208295690602659841","sort":10,"metInformationId":"1198801679756087297","panningTime":"22","stutas":"","remarks":"eeeee","applyforCode":"MET201900002","applyforName":"KJFA2019120004","creator":"1177185314804006913","createDate":"2019-12-21 15:59","creatorName":null,"updater":null,"updateDate":null,"startTime":null,"stutasName":null,"bbServiceSupNum":null,"bbAssistSupNum":null,"qqServiceSupNum":null,"qqAssistSupNum":null,"cdServiceSupNum":null,"cdAssistSupNum":null,"throughTime":null,"fileIds":null,"ids":null,"metCode":null,"metAdds":null,"planningStartTime":null,"metName":null,"metType":null,"reasonApplication":null,"reasonApplicationId":null,"nodeName":null,"formId":null,"documentId":null,"changeSortType":null},"MaterialName":"工装采购"}}
}
function showRecommendSuppliers(params){
    return {"code":0,"msg":"操作成功","data":[{
        "corporationName": "杨嘉宇",
        "supcode": "CGB796",
        "supName": "吉林菲尔彻信息技术有限公司",
        "baseName": "公共",
        "socialCreditCode": "9122015158167106",
        "tacticsName": "正式"
    }, {
        "corporationName": "1",
        "supcode": "BA322",
        "supName": "长春市正达永成机电有限公司",
        "baseName": "公共",
        "socialCreditCode": "91220106794411484L",
        "tacticsName": "潜在"
    }, {
        "corporationName": "傅连学",
        "supcode": "CGB1395",
        "supName": "《中国汽车工业年鉴》期刊社",
        "baseName": "公共",
        "socialCreditCode": "1",
        "tacticsName": "潜在"
    }, {
        "corporationName": "吴建会",
        "supcode": "LAH10",
        "supName": "启明信息技术股份有限公司",
        "baseName": "公共",
        "socialCreditCode": "220102723195753",
        "tacticsName": "潜在"
    }, {
        "corporationName": "测试杨嘉宇法人",
        "supcode": "2019110006",
        "supName": "杨嘉宇测试供应商创建",
        "baseName": "公共",
        "socialCreditCode": "2019112001",
        "tacticsName": "潜在"
    }, {
        "corporationName": "1",
        "supcode": "2019110006",
        "supName": "王妍明测试供应商",
        "baseName": "公共",
        "socialCreditCode": "1",
        "tacticsName": "潜在"
    }, {
        "corporationName": "23423",
        "supcode": "BA077",
        "supName": "长春市机械物资销售处",
        "baseName": "公共",
        "socialCreditCode": "234234",
        "tacticsName": "潜在"
    }, {
        "corporationName": "1",
        "supcode": "2019120006",
        "supName": "测试储备池供应商",
        "baseName": "公共",
        "socialCreditCode": "1",
        "tacticsName": "潜在"
    }]}
}

// Mock.mock( url, post/get , 返回的数据)；
Mock.mock('http://localhost:8686/jiefang-conference/showBasicInfo', 'post', showBasicInfo)
Mock.mock('http://localhost:8686/jiefang-conference/showRecommendSuppliers', 'post', showRecommendSuppliers)
Mock.mock('http://localhost:8686/jiefang-conference/showApproval', 'post', showApproval)
Mock.mock('http://localhost:8686/jiefang-conference/metapplyfor/getMetFild', 'post', getMetFild)
Mock.mock('http://localhost:8686/jiefang-conference/metapplyfor/findAttachment', 'post', findAttachment)
Mock.mock('http://localhost:8686/jiefang-conference/metapplyfor/showResult', 'post', showResult)
Mock.mock('http://localhost:8686/jiefang-conference/metapplyfor/showConchange', 'post', showConchange)
Mock.mock('http://localhost:8686/jiefang-conference/metapplyfor/findFrame/1024efae-8d5d-46ea-8419-574846b405f1', 'post', findFrame)
//Mock.mock('http://localhost:8686/jiefang-conference/queryMeetingTypes', 'post', queryMeetingTypes)
// Mock.mock('http://localhost:8080/jiefang-conference/queryAvbTopics', 'post', queryAvbTopics)