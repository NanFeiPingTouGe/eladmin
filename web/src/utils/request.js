import axios from 'axios'
import Cookies from 'js-cookie'
import router from '@/router'
import qs from 'qs'
import { clearLoginInfo } from '@/utils'
import isPlainObject from 'lodash/isPlainObject'
import merge from 'lodash/merge'
import { Message } from 'element-ui'

const http = axios.create({
    baseURL: window.SITE_CONFIG['apiURL'],
    timeout: 1000 * 180,
    withCredentials: true
})

axios.interceptors.request.use(config => {

    config.headers['Accept-Language'] = Cookies.get('language') || 'zh-CN'
    config.headers['token'] = Cookies.get('token') || ''
    config.headers['userId'] = Cookies.get('userId') || ''
    config.headers['loginFlag'] = Cookies.get('loginFlag') || ''
        // if (Cookies.get("token") == null || Cookies.get("token") == '') {
        //     alert("请找开发查看token丢失情况")
        // }
        // 默认参数
    var defaults = {}
        // 防止缓存，GET请求默认带_t参数
    if (config.method === 'get') {
        config.params = {
            ...config.params,
            ... { '_t': new Date().getTime() }
        }
    }
    if (isPlainObject(config.params)) {
        config.params = {
            ...defaults,
            ...config.params
        }
    }
    if (isPlainObject(config.data)) {
        config.data = {
            ...defaults,
            ...config.data
        }
        if (/^application\/x-www-form-urlencoded/.test(config.headers['content-type'])) {
            config.data = qs.stringify(config.data)
        }
    }

    return config
}, error => {
    return Promise.reject(error)
})

/**
 * 响应拦截
 */
axios.interceptors.response.use(response => {
    //密码和初始密码一致，重定向到修改密码页面
    if (response.data.code === 70000){
        return window.location.href = window.location.origin + "/#/changePassword";  
    }
    if (response.data.code === 401 || response.data.code === 10001) {
        clearLoginInfo()
        router.replace({ name: 'login' })
        return Promise.reject(response.data.msg)
    }
    return response;
}, error => {
    if (error && error.response) {
        let code = error.response.code;
        if (code) {
            //400错误，跳至404页
            switch (code) {
                case 400:
                case 404:
                    //window.location.href = window.location.origin + "/#/404";
                    break;
                default:
                    //后台出异常，统一弹框
                    Message.error(error.response.msg);
                    break;
            }
        }
    }

    return Promise.reject(error)
})

/**
 * 请求拦截
 */
http.interceptors.request.use(config => {
    config.headers['Accept-Language'] = Cookies.get('language') || 'zh-CN'
    config.headers['token'] = Cookies.get('token') || ''
    config.headers['userId'] = Cookies.get('userId') || ''
    config.headers['loginFlag'] = Cookies.get('loginFlag') || ''
        // if (Cookies.get("token") == null || Cookies.get("token") == '') {
        //     alert("请找开发查看token丢失情况")
        // }
        // 默认参数
    var defaults = {}
        // 防止缓存，GET请求默认带_t参数
    if (config.method === 'get') {
        config.params = {
            ...config.params,
            ... { '_t': new Date().getTime() }
        }
    }
    if (isPlainObject(config.params)) {
        config.params = {
            ...defaults,
            ...config.params
        }
    }
    if (isPlainObject(config.data)) {
        config.data = {
            ...defaults,
            ...config.data
        }
        if (/^application\/x-www-form-urlencoded/.test(config.headers['content-type'])) {
            config.data = qs.stringify(config.data)
        }
    }

    return config
}, error => {
    return Promise.reject(error)
})


/**
 * 响应拦截
 */
http.interceptors.response.use(response => {
    
    //捕获异常信息--lp
    // if(response.data.errorState){
    //     Message.error({
    //         message:response.data.errorState,
    //         duration:1000*10
    //     })
    // }
    //密码和初始密码一致，重定向到修改密码页面
    if (response.data.code === 70000){
        return window.location.href = window.location.origin + "/#/changePassword";  
    }
    if (response.data.code === 401 || response.data.code === 10001) {
        clearLoginInfo()
        router.replace({ name: 'login' })
        return Promise.reject(response.data.msg)
    }
    return response;
}, error => {
    if (error && error.response) {
        let code = error.response.code;
        if (code) {
            //400错误，跳至404页
            switch (code) {
                case 400:
                case 404:
                    //window.location.href = window.location.origin + "/#/404";
                    break;
                default:
                    //后台出异常，统一弹框
                    Message.error(error.response.msg);
                    break;
            }
        }
    }

    return Promise.reject(error)
})


http.adornUrl = (actionName) => {
    // 非生产环境 && 开启代理, 接口前缀统一使用[/proxyApi/]前缀做代理拦截!
    return (process.env.NODE_ENV !== 'production' && process.env.OPEN_PROXY ? '/proxyApi/' : window.SITE_CONFIG.baseUrl) + actionName
}

/**
 * get请求参数处理
 * @param {*} params 参数对象
 * @param {*} openDefultParams 是否开启默认参数?
 */
http.adornParams = (params = {}, openDefultParams = true) => {
    var defaults = {
        't': new Date().getTime()
    }
    return openDefultParams ? merge(defaults, params) : params
}

/**
 * post请求数据处理
 * @param {*} data 数据对象
 * @param {*} openDefultdata 是否开启默认数据?
 * @param {*} contentType 数据格式
 * json: 'application/json; charset=utf-8'
 * form: 'application/x-www-form-urlencoded; charset=utf-8'
 */
http.adornData = (data = {}, openDefultdata = true, contentType = 'json') => {
    var defaults = {
        't': new Date().getTime()
    }
    data = openDefultdata ? merge(defaults, data) : data
    return contentType === 'json' ? JSON.stringify(data) : qs.stringify(data)
}

export default http