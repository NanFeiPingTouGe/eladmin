import http from '@/utils/request'
import Cookies from "js-cookie";

export function getDict(Type) {
    // return http({
    //   url: this.baseUrl.JMPS_BAS_URL+`/common/common/${Type}`,
    //   methods: 'get'
    // })
    return http.get(this.baseUrl.JMPS_ADMIN_URL + `/common/common/${Type}`)


}
// 查看erp附件
export function onSeebyERP(url, oldName) {
    console.log(oldName)
    this.axios({
        url: this.baseUrl.serviceUrl1 +
            this.baseUrl.JMPS_FILE_URL +
            `/basAttachment/basattachment/downloadFileByUrl?token=${Cookies.get(
          "token"
        )}`,
        method: "post",
        params: {
            url: url
        },
        responseType: "blob"
    }).then(res => {
        let blob = new Blob([res.data], { type: "multipart/form-data" });
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, oldName);
        } else {
            var link = document.createElement("a");
            link.href = window.URL.createObjectURL(blob);
            link.download = oldName;
            link.click();
            window.URL.revokeObjectURL(link.href);
        }
    });
}
export function onSeebyPaper(id, fileName, extension) {
    this.dataListLoading = true;
    this.loading = true;
    this.axios({
        url: this.baseUrl.serviceUrl1 +
            this.baseUrl.JMPS_FILE_URL +
            `/basAttachment/basattachment/downloadFile?token=${Cookies.get(
          "token"
        )}`,
        method: "post",
        params: {
            id: id
        },
        responseType: "blob"
    }).then(res => {
        let blob = new Blob([res.data], { type: "multipart/form-data" });
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName + extension);
        } else {
            var link = document.createElement("a");
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName + extension;
            link.click();
            window.URL.revokeObjectURL(link.href);
        }
        this.loading = false;
        this.dataListLoading = false;
    });

}
export function getNumFormat(value) {
    if (!value) return '0.00';

    /*原来用的是Number(value).toFixed(0)，这样取整时有问题，例如0.51取整之后为1，感谢Nils指正*/
    /*后来改成了 Number(value)|0,但是输入超过十一位就为负数了，具体见评论 */
    var intPart = Number(value) - Number(value) % 1; //获取整数部分（这里是windy93的方法）
    var intPartFormat = intPart.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,'); //将整数部分逢三一断

    var floatPart = ".00"; //预定义小数部分
    var value2Array = value.toString().split(".");

    //=2表示数据有小数位
    if (value2Array.length == 2) {
        floatPart = value2Array[1].toString(); //拿到小数部分

        if (floatPart.length == 1) { //补0,实际上用不着
            return intPartFormat + "." + floatPart + '0';
        } else {
            return intPartFormat + "." + floatPart;
        }

    } else {
        return intPartFormat + floatPart;
    }

}
export function inputChange(e) {
    var self = this
    var obj = e.target
        //如果用户第一位输入的是小数点，则重置输入框内容
    if (obj.value != '' && obj.value.substr(0, 1) == '.') {
        obj.value = "";
    }
    obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1'); //粘贴不生效
    obj.value = obj.value.replace(/[^\d.,]/g, ""); //清除“数字”和“.”以外的字符
    obj.value = obj.value.replace(/\.{2,}/g, "."); //只保留第一个. 清除多余的
    obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d\d\d).*$/, '$1$2.$3');//只能输入4位小数
    if (obj.value.indexOf(".") < 0 && obj.value != "") { //以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
        if (obj.value.substr(0, 1) == '0' && obj.value.length == 2) {
            obj.value = obj.value.substr(1, obj.value.length);
        }
    }

}

// 2020-0609 验证密码规则
export function validatePwd(password) {
    var iNow = 0;
    if (password.match(/\s/)) {
        return false;
    }
    if (password.match(/[\u4E00-\u9FA5]/g)) {
        return false;
    }
    if (password.match(/[0-9]/g)) {
        iNow++;
    }
    if (password.match(/[a-z]/ig)) {
        iNow++;
    }
    if (password.match(/[A-Z]/)) {
        iNow++;
    }

    if (password.match(/[~`_+=!@#$%^&*-./,<>;:'"]/g)) {
        iNow++;
    }

    if (iNow < 4) {
        return false;
    }
    return true;
}

export function numAdd(num1,  num2)  {    //将传入的数字格式化防止出错
    var r1, r2, m, n;
    try {
        r1 = num1.toString().split(".")[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 =  num2.toString().split(".")[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2))
    n = (r1 >= r2) ? r1 : r2;
    return ((num1 * m +  num2 * m) / m).toFixed(n);
}
//
export function numProduct(num1,  num2)  { 
    if (num1)  {
        //分别获取传入的两个数字的小数点后的位数
        var ext1Len = num1.toString().split('.')[1] ? num1.toString().split('.')[1].length + 1 : 0;
        var ext2Len = num2.toString().split('.')[1] ? num2.toString().split('.')[1].length + 1 : 0;

        //获取10的最大位数幂
        var m = Math.pow(10, Math.max(ext1Len, ext2Len));

        //根据计算公式得出结果

        return (num1 * m) * num2 * m / m / m;
    } else {
        return 0;
    }

}
//判断是否包含汉字
export function CheckChinese(val){

    var reg = new RegExp("[\\u4E00-\\u9FFF]+","g");
    if(reg.test(val)){
        return true;
    }else{
        return false;
    }


}
//判断是否全英文
export function CheckLetter(val){

    var reg = new RegExp("^[a-zA-Z]+$","g");
    if(reg.test(val)){
        return true;
    }else{
        return false;
    }


}

//四位金钱正则  DC   第一位1-9第二位0-9匹配一次或多次小数点后0-9最少匹配1次至多4次或匹配一次0或第一位0到9小数点后0-9最少匹配1次至多4次 刚写测出问题再改
export const fourMoneyReg =/(^[1-9]([0-9]+)?(\.[0-9]{1,4})?$)|(^(0){1}$)|(^[0-9]\.[0-9]{1,4}$)/;

//工作流  流程key
export const activitiProcessKey ={
    //分隔符
    DelimiterL:'.',
    //项目申报
    projectReport:'processOne',
    //需求申报
    demandDeclaration:'process',
    //计划会签
    planCountersignature:'jhhq',
    //计划发布
    plannedRelease:'jhfb',
}