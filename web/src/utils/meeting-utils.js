/**
 * 根据int类型，将meetingtype转换json对象
 * {
 * code: meetingType
 * name: description
 * }
 */
function getTypeJson(meetingType){
    if(!meetingType){
        return null;
    }
    let jsonObj = {code: meetingType};
    switch(meetingType.toString()){
        case '1':
        jsonObj.name = '商务例会';
        break;
        case '2':
        jsonObj.name = '采购委员会';
        break;
        default: break;
    }
    return jsonObj;
}

/**
 * 根据int类型，将meetingStatus转换为文字
 */
function getStatusJson(meetingStatus){
    if(!meetingStatus){
        return null;
    }
    let jsonObj = {code: meetingStatus};
    switch(meetingStatus.toString()){
        case '0':
        jsonObj.name = '会议准备';
        break;
        case '1':
        jsonObj.name = '会议开始';
        break;
        case '2':
        jsonObj.name = '会议结束';
        break;
        default: break;
    }
    return jsonObj;
}

export default {
    getTypeJson, 
    getStatusJson
}