import http from '@/utils/request'

export function getDict (Type) {
    // return http({
    //   url: this.baseUrl.JMPS_ADMIN_URL + `/common/common/${Type}`,
    //   methods: 'get'
    // })

    return http.get(this.baseUrl.JMPS_BAS_URL + `/material/material/${Type}`)
  }