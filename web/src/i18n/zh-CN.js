const t = {}

t.loading = '加载中...'

t.brand = {}
t.brand.lg = '投资项目管理'
t.brand.mini = 'IPM'

t.add = '新增'
t.delete = '删除'
t.deleteBatch = '删除'
t.update = '修改'
t.query = '查询'
t.export = '导出'
t.handle = '操作'
t.confirm = '确定'
t.cancel = '取消'
t.logout = '退出'
t.manage = '处理'
t.submission = '提交'

t.prompt = {}
t.prompt.title = '提示'
t.prompt.info = '确定进行[{handle}]操作?'
t.prompt.success = '操作成功'
t.prompt.failed = '操作失败'
t.prompt.deleteBatch = '请选择删除项'

t.validate = {}
t.validate.required = '必填项不能为空'
t.validate.format = '{attr}格式错误'
//金钱值
t.validate.fourMoneyRegMessage = '正整数或最多四位小数'

t.upload = {}
t.upload.text = '将文件拖到此处，或<em>点击上传</em>'
t.upload.tip = '只支持{format}格式文件！'
t.upload.button = '点击上传'

t.datePicker = {}
t.datePicker.range = '至'
t.datePicker.start = '开始日期'
t.datePicker.end = '结束日期'

t.fullscreen = {}
t.fullscreen.prompt = '您的浏览器不支持此操作'

t.updatePassword = {}
t.updatePassword.title = '修改密码'
t.updatePassword.username = '账号'
t.updatePassword.password = '原密码'
t.updatePassword.newPassword = '新密码'
t.updatePassword.comfirmPassword = '确认密码'
t.updatePassword.validate = {}
t.updatePassword.validate.comfirmPassword = '确认密码与新密码输入不一致'

t.contentTabs = {}
t.contentTabs.closeCurrent = '关闭当前标签页'
t.contentTabs.closeOther = '关闭其它标签页'
t.contentTabs.closeAll = '关闭全部标签页'

/* 页面 */
t.notFound = {}
t.notFound.desc = '抱歉！您访问的页面<em>失联</em>啦...'
t.notFound.back = '上一页'
t.notFound.home = '首页'

t.login = {}
t.login.title = '登录'
t.login.username = '用户名'
t.login.password = '密码'
t.login.captcha = '验证码'
t.login.demo = '在线演示'
t.login.copyright = '人人开源'

t.home = {}
t.home.sysInfo = {}
t.home.sysInfo.name = '系统名称'
t.home.sysInfo.nameVal = 'renren-security【企业版】'
t.home.sysInfo.version = '版本信息'
t.home.sysInfo.versionVal = window.SITE_CONFIG['version']
t.home.sysInfo.osName = '操作系统'
t.home.sysInfo.osVersion = '系统版本'
t.home.sysInfo.osArch = '系统架构'
t.home.sysInfo.processors = 'CPU核数'
t.home.sysInfo.totalPhysical = '系统内存'
t.home.sysInfo.freePhysical = '剩余内存'
t.home.sysInfo.memoryRate = '内存使用'
t.home.sysInfo.userLanguage = '系统语言'
t.home.sysInfo.jvmName = 'JVM信息'
t.home.sysInfo.javaVersion = 'JVM版本'
t.home.sysInfo.javaHome = 'JAVA_HOME'
t.home.sysInfo.userDir = '工作目录'
t.home.sysInfo.javaTotalMemory = 'JVM占用内存'
t.home.sysInfo.javaFreeMemory = 'JVM空闲内存'
t.home.sysInfo.javaMaxMemory = 'JVM最大内存'
t.home.sysInfo.userName = '当前用户'
t.home.sysInfo.systemCpuLoad = 'CPU负载'
t.home.sysInfo.userTimezone = '系统时区'

/* 模块 */
t.model = {}
t.model.name = '名称'
t.model.key = '标识'
t.model.version = '版本'
t.model.createTime = '创建时间'
t.model.lastUpdateTime = '更新时间'
t.model.design = '在线设计'
t.model.deploy = '部署'
t.model.description = '描述'

t.process = {}
t.process.name = '名称'
t.process.key = '标识'
t.process.deployFile = '部署流程文件'
t.process.id = '流程ID'
t.process.deploymentId = '部署ID'
t.process.version = '版本'
t.process.resourceName = 'XML'
t.process.diagramResourceName = '图片'
t.process.deploymentTime = '部署时间'
t.process.active = '激活'
t.process.suspend = '挂起'
t.process.convertToModel = '转换为模型'

t.running = {}
t.running.id = '实例ID'
t.running.definitionKey = '定义Key'
t.running.processDefinitionId = '定义ID'
t.running.processDefinitionName = '定义名称'
t.running.activityId = '当前环节'
t.running.suspended = '是否挂起'
t.running.suspended0 = '否'
t.running.suspended1 = '是'

/* 模块 */
t.model = {}
t.model.name = '名称'
t.model.key = '标识'
t.model.version = '版本'
t.model.createTime = '创建时间'
t.model.lastUpdateTime = '更新时间'
t.model.design = '在线设计'
t.model.deploy = '部署'
t.model.description = '描述'

t.process = {}
t.process.name = '名称'
t.process.key = '标识'
t.process.deployFile = '部署流程文件'
t.process.id = '流程ID'
t.process.deploymentId = '部署ID'
t.process.version = '版本'
t.process.resourceName = 'XML'
t.process.diagramResourceName = '图片'
t.process.deploymentTime = '部署时间'
t.process.active = '激活'
t.process.suspend = '挂起'
t.process.convertToModel = '转换为模型'
t.process.bizRouteSet = '配置业务路由'
t.process.bizRoute = '业务路由'

t.running = {}
t.running.id = '实例ID'
t.running.definitionKey = '定义Key'
t.running.processDefinitionId = '定义ID'
t.running.processDefinitionName = '流程名称'
t.running.activityId = '当前环节'
t.running.suspended = '是否挂起'
t.running.suspended0 = '否'
t.running.suspended1 = '是'

t.news = {}
t.news.title = '标题'
t.news.pubDate = '发布时间'
t.news.createDate = '创建时间'
t.news.content = '内容'

t.schedule = {}
t.schedule.beanName = 'bean名称'
t.schedule.beanNameTips = 'spring bean名称, 如: testTask'
t.schedule.pauseBatch = '暂停'
t.schedule.resumeBatch = '恢复'
t.schedule.runBatch = '执行'
t.schedule.log = '日志列表'
t.schedule.params = '参数'
t.schedule.cronExpression = 'cron表达式'
t.schedule.cronExpressionTips = '如: 0 0 12 * * ?'
t.schedule.remark = '备注'
t.schedule.status = '状态'
t.schedule.status0 = '暂停'
t.schedule.status1 = '正常'
t.schedule.statusLog0 = '失败'
t.schedule.statusLog1 = '成功'
t.schedule.pause = '暂停'
t.schedule.resume = '恢复'
t.schedule.run = '执行'
t.schedule.jobId = '任务ID'
t.schedule.times = '耗时(单位: 毫秒)'
t.schedule.createDate = '执行时间'

t.mail = {}
t.mail.name = '名称'
t.mail.config = '邮件配置'
t.mail.subject = '主题'
t.mail.createDate = '创建时间'
t.mail.send = '发送邮件'
t.mail.content = '内容'
t.mail.smtp = 'SMTP'
t.mail.port = '端口号'
t.mail.username = '邮箱账号'
t.mail.password = '邮箱密码'
t.mail.mailTo = '收件人'
t.mail.mailCc = '抄送'
t.mail.params = '模板参数'
t.mail.paramsTips = '如：{"code": "123456"}'
t.mail.templateId = '模版ID'
t.mail.status = '状态'
t.mail.status0 = '发送失败'
t.mail.status1 = '发送成功'
t.mail.mailFrom = '发送者'
t.mail.createDate = '发送时间'

t.sms = {}
t.sms.mobile = '手机号'
t.sms.status = '状态'
t.sms.status0 = '发送失败'
t.sms.status1 = '发送成功'
t.sms.config = '短信配置'
t.sms.send = '发送短信'
t.sms.platform = '平台类型'
t.sms.platform1 = '阿里云'
t.sms.platform2 = '腾讯云'
t.sms.params = '参数'
t.sms.paramsTips = '如：{"code": "123456"}'
t.sms.params1 = '参数1'
t.sms.params2 = '参数2'
t.sms.params3 = '参数3'
t.sms.params4 = '参数4'
t.sms.createDate = '发送时间'
t.sms.aliyunAccessKeyId = 'Key'
t.sms.aliyunAccessKeyIdTips = '阿里云AccessKeyId'
t.sms.aliyunAccessKeySecret = 'Secret'
t.sms.aliyunAccessKeySecretTips = '阿里云AccessKeySecret'
t.sms.aliyunSignName = '短信签名'
t.sms.aliyunTemplateCode = '短信模板'
t.sms.aliyunTemplateCodeTips = '短信模板CODE'
t.sms.qcloudAppId = 'AppId'
t.sms.qcloudAppIdTips = '腾讯云AppId'
t.sms.qcloudAppKey = 'AppKey'
t.sms.qcloudAppKeyTips = '腾讯云AppKey'
t.sms.qcloudSignName = '短信签名'
t.sms.qcloudTemplateId = '短信模板'
t.sms.qcloudTemplateIdTips = '短信模板ID'

t.oss = {}
t.oss.config = '云存储配置'
t.oss.upload = '上传文件'
t.oss.url = 'URL地址'
t.oss.createDate = '创建时间'
t.oss.type = '类型'
t.oss.type1 = '七牛'
t.oss.type2 = '阿里云'
t.oss.type3 = '腾讯云'
t.oss.type4 = 'FastDFS'
t.oss.type5 = '本地上传'
t.oss.qiniuDomain = '域名'
t.oss.qiniuDomainTips = '七牛绑定的域名'
t.oss.qiniuPrefix = '路径前缀'
t.oss.qiniuPrefixTips = '不设置默认为空'
t.oss.qiniuAccessKey = 'AccessKey'
t.oss.qiniuAccessKeyTips = '七牛AccessKey'
t.oss.qiniuSecretKey = 'SecretKey'
t.oss.qiniuSecretKeyTips = '七牛SecretKey'
t.oss.qiniuBucketName = '空间名'
t.oss.qiniuBucketNameTips = '七牛存储空间名'
t.oss.aliyunDomain = '域名'
t.oss.aliyunDomainTips = '阿里云绑定的域名，如：http://cdn.renren.io'
t.oss.aliyunPrefix = '路径前缀'
t.oss.aliyunPrefixTips = '不设置默认为空'
t.oss.aliyunEndPoint = 'EndPoint'
t.oss.aliyunEndPointTips = '阿里云EndPoint'
t.oss.aliyunAccessKeyId = 'AccessKeyId'
t.oss.aliyunAccessKeyIdTips = '阿里云AccessKeyId'
t.oss.aliyunAccessKeySecret = 'AccessKeySecret'
t.oss.aliyunAccessKeySecretTips = '阿里云AccessKeySecret'
t.oss.aliyunBucketName = 'BucketName'
t.oss.aliyunBucketNameTips = '阿里云BucketName'
t.oss.qcloudDomain = '域名'
t.oss.qcloudDomainTips = '腾讯云绑定的域名'
t.oss.qcloudPrefix = '路径前缀'
t.oss.qcloudPrefixTips = '不设置默认为空'
t.oss.qcloudAppId = 'AppId'
t.oss.qcloudAppIdTips = '腾讯云AppId'
t.oss.qcloudSecretId = 'SecretId'
t.oss.qcloudSecretIdTips = '腾讯云SecretId'
t.oss.qcloudSecretKey = 'SecretKey'
t.oss.qcloudSecretKeyTips = '腾讯云SecretKey'
t.oss.qcloudBucketName = 'BucketName'
t.oss.qcloudBucketNameTips = '腾讯云BucketName'
t.oss.qcloudRegion = '所属地区'
t.oss.qcloudRegionTips = '请选择'
t.oss.qcloudRegionBeijing1 = '北京一区（华北）'
t.oss.qcloudRegionBeijing = '北京'
t.oss.qcloudRegionShanghai = '上海（华东）'
t.oss.qcloudRegionGuangzhou = '广州（华南）'
t.oss.qcloudRegionChengdu = '成都（西南）'
t.oss.qcloudRegionChongqing = '重庆'
t.oss.qcloudRegionSingapore = '新加坡'
t.oss.qcloudRegionHongkong = '香港'
t.oss.qcloudRegionToronto = '多伦多'
t.oss.qcloudRegionFrankfurt = '法兰克福'
t.oss.localDomain = '域名'
t.oss.localDomainTips = '绑定的域名，如：http://cdn.renren.io'
t.oss.fastdfsDomain = '域名'
t.oss.fastdfsDomainTips = '绑定的域名，如：http://cdn.renren.io'
t.oss.localPrefix = '路径前缀'
t.oss.localPrefixTips = '不设置默认为空'
t.oss.localPath = '存储目录'
t.oss.localPathTips = '如：D:/upload'

t.dept = {}
t.dept.name = '名称'
t.dept.parentName = '上级部门'
t.dept.sort = '排序'
t.dept.parentNameDefault = '一级部门'

t.dict = {}
t.dict.dictName = '名称'
t.dict.dictType = '类型'
t.dict.dictValue = '值'
t.dict.sort = '排序'
t.dict.remark = '备注'
t.dict.createDate = '创建时间'

t.logError = {}
t.logError.requestUri = '请求URI'
t.logError.requestMethod = '请求方式'
t.logError.requestParams = '请求参数'
t.logError.ip = '操作IP'
t.logError.userAgent = '用户代理'
t.logError.createDate = '创建时间'
t.logError.errorInfo = '异常信息'

t.logLogin = {}
t.logLogin.creatorName = '用户名'
t.logLogin.status = '状态'
t.logLogin.status0 = '失败'
t.logLogin.status1 = '成功'
t.logLogin.status2 = '账号已锁定'
t.logLogin.operation = '操作类型'
t.logLogin.operation0 = '登录'
t.logLogin.operation1 = '退出'
t.logLogin.ip = '操作IP'
t.logLogin.userAgent = 'User-Agent'
t.logLogin.createDate = '创建时间'

t.logOperation = {}
t.logOperation.status = '状态'
t.logOperation.status0 = '失败'
t.logOperation.status1 = '成功'
t.logOperation.creatorName = '用户名'
t.logOperation.operation = '用户操作'
t.logOperation.requestUri = '请求URI'
t.logOperation.requestMethod = '请求方式'
t.logOperation.requestParams = '请求参数'
t.logOperation.requestTime = '请求时长'
t.logOperation.ip = '操作IP'
t.logOperation.userAgent = 'User-Agent'
t.logOperation.createDate = '创建时间'

t.menu = {}
t.menu.name = '名称'
t.menu.icon = '图标'
t.menu.type = '类型'
t.menu.type0 = '菜单'
t.menu.type1 = '按钮'
t.menu.sort = '排序'
t.menu.url = '路由'
t.menu.permissions = '授权标识'
t.menu.permissionsTips = '多个用逗号分隔，如：sys:menu:save,sys:menu:update'
t.menu.parentName = '上级菜单'
t.menu.parentNameDefault = '一级菜单'
t.menu.resource = '授权资源'
t.menu.resourceUrl = '资源URL'
t.menu.resourceMethod = '请求方式'
t.menu.resourceAddItem = '添加一项'

t.params = {}
t.params.paramCode = '编码'
t.params.paramValue = '值'
t.params.remark = '备注'

t.role = {}
t.role.name = '名称'
t.role.remark = '备注'
t.role.createDate = '创建时间'
t.role.menuList = '菜单授权'
t.role.deptList = '数据授权'

t.user = {}
t.user.username = '用户名'
t.user.deptName = '所属部门'
t.user.email = '邮箱'
t.user.mobile = '手机号'
t.user.status = '状态'
t.user.status0 = '停用'
t.user.status1 = '正常'
t.user.createDate = '创建时间'
t.user.password = '密码'
t.user.comfirmPassword = '确认密码'
t.user.realName = '真实姓名'
t.user.gender = '性别'
t.user.gender0 = '男'
t.user.gender1 = '女'
t.user.gender2 = '保密'
t.user.roleIdList = '角色配置'
t.user.validate = {}
t.user.validate.comfirmPassword = '确认密码与密码输入不一致'

t.correction = {}
t.correction.post = '申请岗位'
t.correction.entryDate = '入职日期'
t.correction.correctionDate = '转正日期'
t.correction.workContent = '工作內容'
t.correction.achievement = '工作成绩'

t.process.comment = '审核意见'
t.process.completeTask = '通过'
t.process.rejectTask = '驳回'
t.process.doBackRollback = '回退'
t.process.terminationTask = '终止'
t.process.entrustTask = '委托'
t.process.createInstance = '发起流程'
t.process.instanceId = '流程实例ID'
t.process.taskId = '任务ID'
t.process.days = '天数'
t.process.businessKey = '业务KEY'
t.process.processDefinitionName = '流程名称'
t.process.ended = '是否结束'
t.process.ended0 = '是'
t.process.ended1 = '否'
t.process.startTime = '流程开始时间'
t.process.endTime = '流程结束时间'
t.process.operate = '审批操作'
t.process.activityName = '当前环节'
t.process.createTime = '任务创建时间'
t.process.assignee = '处理人'
t.process.viewFlowImage = '查看'
t.process.flowImage = '流程图'
t.process.processDefinitionVersion = '流程版本'
t.process.startUserId = '发起人'
t.process.taskName = '任务名称'
t.process.owner = '任务所有人'
t.process.claim = '签收'
t.process.routeError = '请先配置业务表单路由信息'
t.process.entrustError = '请选择委托人'
t.process.formURLError = '请设置保存表单的URL'
t.process.keyError = '请设置流程KEY'
t.process.formNameError = '请设置表单名称'
t.process.businessKeyError = '业务KEY为空，无法启动流程'
t.process.notExistError = '没有查询到流程，请先设计流程'
t.process.circulation = '流转详情'
t.process.againMessage = "流程重新发起成功"
t.process.failMessage = "重新发起流程失败"
t.process.startMessage = "确定提交审批吗？"

t.task = {}
t.task.businessKeyError = '业务KEY为空，无法处理任务'
t.task.detailError = '业务KEY为空，无法查看处理详情'
t.task.startTime = '任务开始时间'
t.task.endTime = '任务结束时间'
t.task.durationInSeconds = '任务时长（秒）'
t.all = {}
t.all.success = '{attr}操作成功'
t.all.error = '请填写必填项'
t.all.fileMax = "上传文件最大支持150M"
    //会议
t.meeting = {}
t.meeting.activityFailed = "工作流待办返回失败"
t.meeting.noProcessing = '请先处理未汇报的上会申请'
t.meeting.updataMet = '请选择想要更改的会议议题'
t.meeting.olnyMeeT = '请选择一条会议'
t.meeting.fileNum = "请选择文件"

//会议中心meetingdemo
t.meetingdemo = {}
t.meetingdemo.attenderRequired = '请选择出席人'
t.meetingdemo.roomRequired = '请选择会议室'
t.meetingdemo.cancelReason = '请录入取消原因'
t.meetingdemo.cancelSuccess = '取消成功'

//申请
t.req = {}
    //编辑和新增
t.req.biLu = '请填写必录项'
t.req.approvalForm = '请上传经审批的一般材料采购申请单'
t.req.addUpdataSubmission = '提交成功，可以到查询采购申请中查看采购申请状态'
t.req.selectMateriel = '请选择物料一条以上是的物料信息'
t.req.applySubmission = '编号为{attr1}采购申请提交成功'
t.req.savaApply = '请先保存采购申请'
t.req.biLud = '请填写明细价格，单位，到货日期'
t.req.OnlyPersonInCharge = "请选择一名用户"

//分配
t.req.distribution = '确定分配采购申请吗?'
t.req.distributionSuccess = '分配成功'
t.req.differentSources = '请选择同一来源进行分配'
t.req.distributionState = '请先选择分配状态为退回或待分配的采购申请进行分配'
t.req.distributionBuyerUserid = '申请编号为{attr1}行号为{attr2}请先选择采购员，再分配'
t.req.distributionDetailedStatus = '申请编号为{attr1}行号为{attr2}采购申请不可以再进行分配'
t.req.distributionReject = '申请编号为{attr}采购申请已经接收无法驳回'
t.req.distributionReject1 = '采购申请已经接收无法驳回'
t.req.sameApplication = '驳回一次只能操作一个采购申请'
t.req.sameApplication1 = '请选择驳回申请'
t.req.distributionDelect = '采购申请删除成功'
t.req.buyerUserid = "请选择采购员"
t.req.checked = '请选择一条采购申请'

//维护项目负责人
t.req.selectThePersonInCharge = '请选择负责人'
t.req.onlyPersonInCharge = '请选择唯一负责人'
t.req.Selectbas = '请选择材料组'
t.req.maintenanceReason = '维护原因后再提交'
t.req.maintenanceReason1 = '请控制反馈内容在500个字以内'
    //采购员列表
t.req.selectApply = '请选择采购申请'
t.req.nohavePerson = '请先维护项目负责人，再启动明细'
t.req.alreadyStarted = '选中明细中存在已启动明细，请重新选择'
t.req.associated = '选中明细中存在已关联明细，请重新选择'
t.req.returnApply = '{attr1}申请状态为{attr2}不可退回'
t.req.terminationApply = '{attr1}申请状态为{attr2}不可终止'
    //申请关联
t.req.selectTwoApply = '请选择一条采购申请'
t.req.selectDetailedStatus = '请选择一条纸质不是待处理状态的采购申请，一条其他来源待处理状态的采购申请'
t.req.notAssociated = '请选择非被关联的采购申请'
t.req.associatedSuccess = '关联成功'
t.req.sameDource = '请选择项目来源的采购申请'

//方案
t.plan = {}
t.plan.please = '请选择{attr}方案'
    //方案补充资料
t.plan.fileType = '请先选择文件类型'
    //框架方案
t.frame = {}
t.frame.submission = '请先保存方案'
t.frame.insertEvent = '{attr}年没有通过的框架方案'
t.frame.noMeeting = '请选择提交已创建会议申请的方案'
t.frame.delSup = ' 请在右侧表格选择想要移除框架方案的供应商'
t.frame.addSup = '请选择新增框架供应商'
t.frame.base = '请选择基地'
    //结果
t.result = {}

//合同
t.con = {}

//供应商
t.sup = {}
t.sup.supChangeMessage = "请先保存再提交"
t.sup.supConMessage = "未获取到与该供应商相关的合同信息"
t.sup.baseInfoMessage = "供应商基础信息保存成功"
t.sup.settlementMessage = "结算信息保存成功"
t.sup.waitMessage = "提交成功，请耐心等待..."
t.sup.mesasge = "当前选中的供应商请码流程正在审核中！请耐心等待"
t.sup.noticeMessage = "当前选中的供应商请码已经是正式码"
t.sup.errorMessage = "当前选中的数据初始状态异常，不能提交审批,请联系运维人员"
t.sup.sendSuccessMessage = "推送成功"
t.sup.sendFailMessage = "推送失败"
t.sup.selectOneMessage = "请选择一条要操作的数据"
t.sup.approveSuccessMessage = "审批成功"
t.sup.approveFailMessage = "审批失败"
t.sup.doNotCommitMessage = "审批流程已经启动,不能重复提交"
t.sup.startMessage = "审批流程已启动"
t.sup.registerMessage = "邀请成功,供应商可以使用邮箱登录系统"
t.sup.doNotEmptyMessage = "保存信息不能为空"
t.sup.sendNoticeMessage = "该供应商的请码流程未通过，不能进行推送！"
t.sup.sendKeepMessage = "该供应商的请码流程未结束，不能进行推送！"
t.sup.fmMessage = "未给该供应商发码，不能进行推送！"
t.sup.uploadMessage = "请选择上传类型后，再进行上传文件操作！"
t.sup.unCommitMessage = "当前选中的供应商未提交请码申请，不能进行推送！"
t.sup.unFmMessage = "当前选中的供应商请码流程还未审批结束，不能进行发码操作！"
t.sup.unStartMessage = "当前选中的供应商未发起请码申请，不能进行发码操作！"
t.sup.unAgainMessage = "不能对已发码的供应商进行重复操作！"
t.sup.out = "确定要进行退组操作吗？"
t.sup.startApprove = "流程发起成功"
t.sup.approvePesonNotNull = "请选择审批人"
t.sup.sendEmailMessage = "您选择的供应商中有未发码的,请重新选择"
t.sup.tacMessage = "当前供应商进组策略不能为正式，请重新选择！"
t.sup.inviteMessage = "您选择的供应商中有已被邀请过的供应商，请重新选择！"
t.sup.notNull = "驳回时，审批意见不能为空！"
t.sup.onlyOne = "每次只能选择一条数据进行操作！"
t.sup.metMessage = "请选择会议"


t.user = {}
t.user.message = "改用不存在"

t.bas = {}

t.material = {}
t.material.addOne = "当前材料组为一级材料组，不能进行新增操作"
t.material.addTwo = "不能新增二级材料组!"
t.material.addMaterialMessage = " 材料组不能为空！"
t.material.tacticsMessage = " 策略不能为空！"
export default t
