import Vue from 'vue'
import Cookies from 'js-cookie'
import Router from 'vue-router'
import http from '@/utils/request'
import { pageRoutes, childrenRoutes, moduleRoutes } from '@/router/config'
import {
    isURL
} from '@/utils/validate'
import baseUrl from '@/config'
// Ima登录验证返回首页解密参数的
import { getUrlKey } from "@/utils/getUrlKey.js";
import { Decrypt, Encrypt,getKey } from '@/utils/my-aes-crypto.js'
Vue.use(Router)

const router = new Router({
    // mode: 'history',
    mode: 'hash',
    scrollBehavior: () => ({
        y: 0
    }),
    routes: pageRoutes.concat(moduleRoutes)
})
export function addDynamicRoute(routeParams, router) {
    // 组装路由名称, 并判断是否已添加, 如是: 则直接跳转
    var routeName = routeParams.routeName
    var dynamicRoute = window.SITE_CONFIG['dynamicRoutes'].filter(item => item.name === routeName)[0]
    if (dynamicRoute) {
        return router.push({
            name: routeName,
            params: routeParams.params
        })
    }
    // 否则: 添加并全局变量保存, 再跳转
    dynamicRoute = {
        path: routeName,
        component: () =>
            import (`@/views/modules/${routeParams.path}`),
        name: routeName,
        meta: {
            ...window.SITE_CONFIG['contentTabDefault'],
            menuId: routeParams.menuId,
            title: `${routeParams.title}`
        }
    }
    router.addRoutes([{
        ...moduleRoutes,
        name: `main-dynamic__${dynamicRoute.name}`,
        children: [dynamicRoute]
    }])
    window.SITE_CONFIG['dynamicRoutes'].push(dynamicRoute)
    router.push({
        name: dynamicRoute.name,
        params: routeParams.params
    })
}

let appendRoutes = (dataList) => {
    for (let i = 0; i < childrenRoutes.length; i++) {
        concatRoutes(dataList, childrenRoutes[i]);
    }

    return dataList;
}

let concatRoutes = (parentList, appendNode) => {
    for (let i = 0; i < parentList.length; i++) {
        let value = parentList[i];

        //向路由的每个结点追加isLastLevel属性，标识是否为最后一级导航结点，按配置到数据库中的算，页面上后append的，默认都是页面内部路由，不算
        if (value.children == null) {
            value.children = [];
        }

        if (value.isLastLevel == null || value.isLastLevel == false) {
            value.isLastLevel = value.children.length == 0;
        }

        //依据rootUrl寻找父级结点
        if (value.url && 　appendNode.rootUrl == value.url) { //找到了，为children赋值，结束循环
            value.children = value.children.concat(appendNode.children);
            return;
        } else { //没找到，递归
            concatRoutes(value.children, appendNode);
        }
    }
}


router.beforeEach((to, from, next) => {
    //加点自定义逻辑 dc 2021 11 03 干掉导航
    if(typeof to.meta.evalFunc == 'function'){
        to.meta.evalFunc(to)
    }
    // 20201109 iam登录验证开始
    //iam OAuth方式登录才通过这种方式获取参数，因为后台将参数加密，前台需要解密
    //获取路径的userId
    var userId = getUrlKey("userId");
    //获取跳转路径的token
    var token = getUrlKey("token");
    //获取跳转路径的key
    var key = getUrlKey("key");
    if (userId && token) {
        userId = userId.replace(/\ +/g, "+");//去掉空格
        userId = userId.replace(/[ ]/g, "+");    //去掉空格
        userId = userId.replace(/[\r\n]/g, "+");//去掉回车换行
        token = token.replace(/\ +/g, "+");//去掉空格
        token = token.replace(/[ ]/g, "+");    //去掉空格
        token = token.replace(/[\r\n]/g, "+");//去掉回车换行
    }
    var deUserId = "";
    var deToken = "";
    console.log("userId", userId);
    console.log("token", token);
    console.log("key", key);
    if( Cookies.get('cookieFlag') != '1'){
        // console.log("时间1==="+Number("123" + getKey()))
        // console.log("时间2==="+Number("123" + key))
        if (userId && token && key && Number("123" + getKey()) - Number("123" + key) < 600000*5) {
            deUserId = Decrypt(userId, "123" + key, "123" + key);
            deToken = Decrypt(token, "123" + key, "123" + key);
            //将token 和userid存入cookier 方便后台AuthorizeFilter获取到
            console.log("userIdRes", deUserId);
            console.log("tokenRes", deToken);
            Cookies.set("token", deToken);
            Cookies.set("userId", deUserId);
        } else {
            console.log("已经超时了11111111");
        }
    }else{
        Cookies.remove('cookieFlag');
    }

    // 20201109 iam登录验证结束


    if (window.SITE_CONFIG['dynamicMenuRoutesHasAdded'] || fnCurrentRouteIsPageRoute(to, pageRoutes)) {
        if(window.SITE_CONFIG['isReLogin'] == false){
            return next()
        }
    }
    window.SITE_CONFIG['isReLogin'] = false;//JEPS-1802
    // 获取菜单列表, 添加并全局变量保存
    http.get(baseUrl.JMPS_ADMIN_URL + '/sys/menu/nav').then(({
        data: res
    }) => {
        if (res.code !== 0) {
            console.log('nav debugger', JSON.stringify(res))
            Vue.prototype.$message.error(res.msg)
            // return next({
            //     name: 'login'
            // })
            return window.location.replace(window.SITE_CONFIG['iamLogin']);
        }
        window.SITE_CONFIG['menuList'] = appendRoutes(res.data);
        fnAddDynamicMenuRoutes(window.SITE_CONFIG['menuList'])
        next({
            ...to,
            replace: true
        })
    }).catch((error) => {
        console.log("校验弱密码error===+"+error)
        if(error == "70000"){
            next({name: 'changePassword'})
        }else{
            next({
                name: 'login'
            })
        }
    })
})

/**
 * 判断当前路由是否为页面路由
 * @param {*} route 当前路由
 * @param {*} pageRoutes 页面路由
 */
function fnCurrentRouteIsPageRoute(route, pageRoutes = []) {
    var temp = []
    for (var i = 0; i < pageRoutes.length; i++) {
        if (route.path === pageRoutes[i].path) {
            return true
        }
        if (pageRoutes[i].children && pageRoutes[i].children.length >= 1) {
            temp = temp.concat(pageRoutes[i].children)
        }
    }
    return temp.length >= 1 ? fnCurrentRouteIsPageRoute(route, temp) : false
}

const createRouteConfig = (menu) => {
    // 组装路由
    var route = {
        url: menu.url,
        path: '',
        component: null,
        name: '',
        meta: {
            ...window.SITE_CONFIG['contentTabDefault'],
            menuId: menu.id,
            title: menu.name
        }
    }

    if (menu.meta && menu.meta.isBreadcrumb) {
        route.meta = {
            ...route.meta,
            ...menu.meta
        }
    }

    // eslint-disable-next-line
    let URL = (menu.url || '').replace(/{{([^}}]+)?}}/g, (s1, s2) => eval(s2)) // URL支持{{ window.xxx }}占位符变量
    if (isURL(URL)) {
        route['path'] = route['name'] = `i-${menu.id}`
        route['meta']['iframeURL'] = URL
    } else {
        URL = URL.replace(/^\//, '').replace(/_/g, '-')
        route['path'] = route['name'] = URL.replace(/\//g, '-')
        route['component'] = () =>
            import (`@/views/modules/${URL}`)
    }

    if (menu.routeName && menu.routeName != "") {
        route['name'] = menu.routeName;
    }

    return route;
}

/**
 * 添加动态(菜单)路由
 * @param {*} menuList 菜单列表
 * @param {*} routes 递归创建的动态(菜单)路由
 */
function fnAddDynamicMenuRoutes(menuList = [], routes = []) {
    var temp = []
    for (var i = 0; i < menuList.length; i++) {

        //逻辑需要，对于其下配置了children的结点，本身也要能够跳转
        if (menuList[i].url && 　menuList[i].children && menuList[i].children.length >= 1) {
            routes.push(createRouteConfig(menuList[i]));
        }

        if (menuList[i].children && menuList[i].children.length >= 1) {
            temp = temp.concat(menuList[i].children)
            continue
        }

        routes.push(createRouteConfig(menuList[i]))
    }
    if (temp.length >= 1) {
        return fnAddDynamicMenuRoutes(temp, routes)
    }
    // 添加路由
    router.addRoutes([{
            ...moduleRoutes,
            name: 'main-dynamic-menu',
            children: routes
        },
        {
            path: '*',
            redirect: {
                name: '404'
            }
        }
    ])
    window.SITE_CONFIG['dynamicMenuRoutes'] = routes
    window.SITE_CONFIG['dynamicMenuRoutesHasAdded'] = true

}


export default router
