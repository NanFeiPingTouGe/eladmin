// 页面路由(独立页面)
export const pageRoutes = [{
    path: '/404',
    component: () =>
        import ('@/views/pages/404'),
    name: '404',
    meta: {
        title: '404未找到'
    },
    beforeEnter(to, from, next) {
        // 拦截处理特殊业务场景
        // 如果, 重定向路由包含__双下划线, 为临时添加路由
        if (/__.*/.test(to.redirectedFrom)) {
            return next(to.redirectedFrom.replace(/__.*/, ''))
        }
        next()
    }
},
    {
        path: '/login',
        component: () =>
            import ('@/views/pages/login'),
        name: 'login',
        meta: {
            title: '登录'
        }
    },
    {
        //li.daming 2020-11-11
        path: '/forgetPassword',
        component: () =>
            import ('@/views/pages/forgetPassword'),
        name: 'forgetPassword',
        meta: {
            title: '忘记密码'
        }
    },
    {
        //li.daming 2020-11-11
        path: '/passwdResetc',
        component: () =>
            import ('@/views/pages/passwdResetc'),
        name: 'passwdResetc',
        meta: {
            title: '重置密码'
        }
    },
    {
        path: '/loginiam',
        component: () =>
            import ('@/views/pages/loginiam'),
        name: 'loginiam',
        meta: {
            title: 'IAM登录'
        }
    },
    {
        path: '/iamUserNotFind',
        component: () =>
            import ('@/views/pages/iamUserNotFind'),
        name: 'iamUserNotFind',
        meta: {
            title: '用户不存在'
        }
    },
    {
        path: '/changePassword',
        component: () =>
            import ('@/views/pages/changePassword'),
        name: 'changePassword',
        meta: {
            title: '更改密码'
        }
    },
    {
        path: '/functionalManagement',
        component: () =>
            import ('@/views/modules/sys/functionalManagement'),
        name: 'functionalManagement',
        meta: {
            title: '功能管理'
        }
    },
]

//dc 20211101
const evalFunc = x => {
    const noUseBread=x.query.noUseBread||x.params.noUseBread||x.query.businessKey;

    if (!!noUseBread) {
        if(x.meta.parentUrl){
            x.meta.evalBackParentUrl__ = x.meta.parentUrl;
        }
        delete x.meta.parentUrl;
        x.params.$notShowBackButton__ = true;
    } else {
        if (x.meta.evalBackParentUrl__&&!x.meta.parentUrl) {
            x.meta.parentUrl = x.meta.evalBackParentUrl__;
        }
    }
}

// 模块路由(基于主入口布局页面)
export const moduleRoutes = {
    path: '/',
    component: () =>
        import ('@/views/main'),
    name: 'main',
    redirect: {
        name: 'home'
    },
    meta: {
        title: '主入口布局'
    },
    children: [{
        path: '/home',
        component: () =>
            import ('@/views/modules/home'),
        name: 'home',
        meta: {
            title: '首页',
            isTab: true
        }
    },
    ]
}
export const childrenRoutes = {}