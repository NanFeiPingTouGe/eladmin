//流程类型


/**
 * 议题类型
 */
//采购方案
const TOPIC_TYPE_PLAN = "0";
//采购结果
const TOPIC_TYPE_RESULT = "1";
//合同变更
const TOPIC_TYPE_CHANGE = "2";
//统一上会
const TOPIC_TYPE_UNIFY = "3";
//其它类型
const TOPIC_TYPE_OTHER = "4";
//框架方案
const TOPIC_TYPE_FRAME = "5";
export const TOPIC_TYPE = {
    PLAN: TOPIC_TYPE_PLAN,
    RESULT: TOPIC_TYPE_RESULT,
    CHANGE: TOPIC_TYPE_CHANGE,
    UNIFY: TOPIC_TYPE_UNIFY,
    OTHER: TOPIC_TYPE_OTHER,
    FRAME: TOPIC_TYPE_FRAME
}
