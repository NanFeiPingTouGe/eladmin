//根据环境，将apiURL赋不同的值
switch (process.env.VUE_APP_NODE_ENV) {
    case 'dev': //<!-- 开发环境 -->
        window.SITE_CONFIG['apiURL'] = 'http://localhost:8686/'
        window.SITE_CONFIG['iamLogin'] = "https://iamuat.fawjiefang.com.cn/oauth2/rest/authz?response_type=code&client_id=a1088a5daab24e92ab7f618dae95096a&domain=IdmDomain&state=xyz&scope=IdmResServer.UserProfile.me%20openid%20email%20phone%20profile&redirect_uri=http://10.43.74.4:8686/jiefang-apigateway/jiefang-admin/iamLogin/validation";
        window.SITE_CONFIG['iamLogOut'] = "https://iamuat.fawjiefang.com.cn/FawIdmCommonUtils/ssoLoginService/goLogout?client_id=2de43e4395814b4784881e42665ef27f";
        window.SITE_CONFIG['updatePassword'] = "https://iamuat.fawjiefang.com.cn/IAMEmployeeExtApp/#/home/user_update_password"
        break;
    case 'sit': //<!-- 集成测试环境 -->
        window.SITE_CONFIG['apiURL'] = 'http://10.60.243.131/jiefang-apigateway/';
        window.SITE_CONFIG['iamLogin'] = "https://iamuat.fawjiefang.com.cn/oauth2/rest/authz?response_type=code&client_id=a1088a5daab24e92ab7f618dae95096a&domain=IdmDomain&state=xyz&scope=IdmResServer.UserProfile.me%20openid%20email%20phone%20profile&redirect_uri=http://10.60.243.34/jiefang-apigateway/jiefang-admin/iamLogin/validation";
        window.SITE_CONFIG['iamLogOut'] = "https://iamuat.fawjiefang.com.cn/FawIdmCommonUtils/ssoLoginService/goLogout?client_id=a1088a5daab24e92ab7f618dae95096a";
        window.SITE_CONFIG['updatePassword'] = "https://iamuat.fawjiefang.com.cn/IAMEmployeeExtApp/#/home/user_update_password"
        break;
    case 'uat': //<!-- 验收测试环境  -->
        window.SITE_CONFIG['apiURL'] = 'http://10.60.25.178/jiefang-apigateway/';
        window.SITE_CONFIG['iamLogin'] = "https://iam.fawjiefang.com.cn/oauth2/rest/authz?response_type=code&client_id=a1088a5daab24e92ab7f618dae95096a&domain=IdmDomain&state=xyz&scope=IdmResServer.UserProfile.me%20openid%20email%20phone%20profile&redirect_uri=http://10.60.243.34/jiefang-apigateway/jiefang-admin/iamLogin/validation";
        window.SITE_CONFIG['iamLogOut'] = "https://iam.fawjiefang.com.cn/FawIdmCommonUtils/ssoLoginService/goLogout?client_id=a1088a5daab24e92ab7f618dae95096a";
        window.SITE_CONFIG['updatePassword'] = "https://iam.fawjiefang.com.cn/IAMEmployeeExtApp/#/home/user_update_password"
        break;
    case 'prod': //<!-- 生产环境 -->
        window.SITE_CONFIG['apiURL'] = 'http://10.60.243.34/jiefang-apigateway/';
        window.SITE_CONFIG['iamLogin'] = "https://iam.fawjiefang.com.cn/oauth2/rest/authz?response_type=code&client_id=a1088a5daab24e92ab7f618dae95096a&domain=IdmDomain&state=xyz&scope=IdmResServer.UserProfile.me%20openid%20email%20phone%20profile&redirect_uri=http://10.60.243.34/jiefang-apigateway/jiefang-admin/iamLogin/validation";
        window.SITE_CONFIG['iamLogOut'] = "https://iam.fawjiefang.com.cn/FawIdmCommonUtils/ssoLoginService/goLogout?client_id=a1088a5daab24e92ab7f618dae95096a";
        window.SITE_CONFIG['updatePassword'] = "https://iam.fawjiefang.com.cn/IAMEmployeeExtApp/#/home/user_update_password"
        break;
    default:
        window.SITE_CONFIG['apiURL'] = 'http://10.60.243.34/jiefang-apigateway/';
        window.SITE_CONFIG['iamLogin'] = "https://iam.fawjiefang.com.cn/oauth2/rest/authz?response_type=code&client_id=a1088a5daab24e92ab7f618dae95096a&domain=IdmDomain&state=xyz&scope=IdmResServer.UserProfile.me%20openid%20email%20phone%20profile&redirect_uri=http://10.60.243.34/jiefang-apigateway/jiefang-admin/iamLogin/validation";
        window.SITE_CONFIG['iamLogOut'] = "https://iam.fawjiefang.com.cn/FawIdmCommonUtils/ssoLoginService/goLogout?client_id=a1088a5daab24e92ab7f618dae95096a";
        window.SITE_CONFIG['updatePassword'] = "https://iam.fawjiefang.com.cn/IAMEmployeeExtApp/#/home/user_update_password"
        break;
}

const DOWNLOAD_URL = "http://localhost:8686/"
const JMPS_ADMIN_URL = "jiefang-admin"
const IPM_PROJECT_URL = "jiefang-project"
const IPM_FILE_URL = "jiefang-file"
const serviceUrl = window.SITE_CONFIG['apiURL']
const serviceUrl1 = window.SITE_CONFIG['apiURL']
const JMPS_ADMIN_PORT = "http://localhost:8088"
const BUSINESS_NUM = {
    WQYYS : "0821",//五千元以上表单
    SBLGXGZ:"0822",//设备类更新改造
    GYYF:"0801",//工艺研发
    JDSBGXZC:"0100",//机动设备更新支出
    JDSBGLZC:"0200",//机动设备改良支出
    GQJ:"0300",//工器具
    JSCSZC:"0400",//技术措施支出
    ZLCSZC:"0500",//质量措施支出
    JZWGZZC:"0600",//建筑物改造支出
    LXJJZC:"0700",//零星基建支出



}
export default {
    JMPS_ADMIN_URL,
    IPM_PROJECT_URL,
    serviceUrl,
    DOWNLOAD_URL,
    IPM_FILE_URL,
    BUSINESS_NUM,//用来存表单名称对应的businessnum
}