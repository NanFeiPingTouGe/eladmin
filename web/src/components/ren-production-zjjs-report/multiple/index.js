import RenProcessMultiple from './multiple/src/ren-process-multiple'

RenProcessMultiple.install = function (Vue) {
  Vue.component(RenProcessMultiple.name, RenProcessMultiple)
}

export default RenProcessMultiple
