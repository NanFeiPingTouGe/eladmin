function exportExcel(header, filterVale, excelName, dataList) {
  // this.downloadLoading = true;
  require.ensure([], () => {
    const { export_json_to_excel } = require("@/excel/Export2Excel");
    const tHeader = header;
  debugger
    const filterVal = filterVale;
  
    const list = dataList;
    const data = formatJson(filterVal, list);
    export_json_to_excel(tHeader, data, excelName);
    this.downloadLoading = false;
  });
}
function formatJson(filterVal, jsonData) {
  return jsonData.map(v => filterVal.map(j => v[j]));
}

export { exportExcel }
