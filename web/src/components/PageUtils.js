  function gettotalPage(list) {
      return list.length
  }

  function getList(list, pageIndex, pageSize) {
      // pageIndex: 1,
      // pageSize: 10,
      var l = [];
      if (pageIndex != 1) {
          pageIndex = (pageIndex - 1) * pageSize;
      } else {
          pageIndex = pageIndex - 1;
      }
      var count = 0;
      for (var i = pageIndex; i < list.length; i++) {
          if (count < pageSize) {
              l.push(list[i])
              count = count + 1;
          } else {
              break;
          }
      }
      return l;
  }
  export { gettotalPage, getList }