import Vue from 'vue'
//import Mock from '@/mock'
import global from '@/config/global'
import baseUrl from '@/config'
import Element from 'element-ui'
import App from '@/App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'
import axios from 'axios'
import '@/icons'
import '@/element-ui/theme/index.css'
import '@/assets/scss/aui.scss'
import http from '@/utils/request'
import { hasPermission } from '@/utils'
import cloneDeep from 'lodash/cloneDeep'
import $ from 'jquery'
import { gettotalPage } from './components/PageUtils'
import { getList } from './components/PageUtils.js'
import { exportExcel } from '@/components/ExportExcel.js'
import { getDict } from '@/utils/common.js'
// 2020-07-24 加法 不失精度
import { numAdd } from '@/utils/common.js'
// 2021-02-26 只能输入4位小数
import { inputChange } from '@/utils/common.js'
// 2021-09-30 四位金钱正则
import { fourMoneyReg } from '@/utils/common.js'
import moment from 'moment'

import '@/utils/directives.js'
import XEUtils from 'xe-utils'
// 2020-0609 验证密码规则
import { validatePwd } from '@/utils/common.js'
import renRadioGroup from '@/components/ren-radio-group'
import renSelect from '@/components/ren-select'
import renProcessMultiple from '@/components/ren-process-multiple'
import renProcessStart from '@/components/ren-process-start'
import renProcessRunning from '@/components/ren-process-running'
import renProcessDetail from '@/components/ren-process-detail'
import renDeptTree from '@/components/ren-dept-tree'
import renRegionTree from '@/components/ren-region-tree'

//引入查询框样式
import '@/css/overall_situation.css'
//import  '@/element-ui/theme/dialog.css'
// 2021-07-15 加法不失精度
Vue.prototype.numAdd = numAdd
Vue.prototype.inputChange = inputChange
Vue.prototype.fourMoneyReg = fourMoneyReg
Vue.prototype.$moment = moment
Vue.use(renRadioGroup)
Vue.use(renSelect)
Vue.use(renDeptTree)
Vue.use(renRegionTree)
Vue.use(renProcessMultiple)
Vue.use(renProcessStart)
Vue.use(renProcessRunning)
Vue.use(renProcessDetail)
Vue.prototype.axios = axios

Vue.config.productionTip = false
Vue.use(Element, {
    size: 'medium',
    i18n: (key, value) => i18n.t(key, value)
})
//时间转换
Vue.filter('operationDate2', function(value) {
    if (!value) {
        return ''
    }
    var d = new Date(value.substring(0,10));

    var times = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate();

    return times;

})
// 挂载全局
Vue.prototype.$http = http
Vue.prototype.$hasPermission = hasPermission
Vue.prototype.$ = $
Vue.prototype.baseUrl = baseUrl
Vue.prototype.getDict = getDict
// 2020-0609 验证密码规则
Vue.prototype.$validatePwd = validatePwd


Vue.prototype.gettotalPage = gettotalPage // 总条数
Vue.prototype.getList =
Vue.prototype.exportExcel = exportExcel //导出

Vue.prototype.downloadUrl = baseUrl.DOWNLOAD_URL //后台服务路径
    // 保存整站vuex本地储存初始状态
window.SITE_CONFIG['storeState'] = cloneDeep(store.state)

new Vue({
    i18n,
    router,
    store,
    render: h => h(App)
}).$mount('#app')
