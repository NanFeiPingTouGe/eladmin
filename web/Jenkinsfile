//需要在jenkins的Credentials设置中配置jenkins-harbor-creds、jenkins-k8s-config参数
pipeline {
    agent any
    //agent { label 'slave136' }
    tools { nodejs 'nodejs8' }

    environment {
        K8S_SIT_CONFIG = credentials('test_ipm_k8s')
        K8S_UAT_CONFIG = credentials('ipm_uat_k8s_token')
        K8S_PROD_CONFIG = credentials('ipm_k8s')

        GIT_TAG = sh(returnStdout: true, script: 'git describe --tags --always').trim()
        GIT_CRED = credentials('suidepeng')
        IMG_TAG = 'latest'
        APP_NAME = 'ipm-web'
        HARBOR_HOST = '10.43.4.130'
        HARBOR_USER = 'suidepeng'
        HARBOR_PSW = credentials('harbor-suidepeng')
    }
    parameters {
        choice(
          description: '只是重新部署',
          name: 'ONLY_DEPLOY',
          choices: ['no', 'yes']
        )

        choice(
          description: '部署到集成测试环境,验收测试环境还是生产环境',
          name: 'TARGET',
          choices: ['sit', 'uat', 'prod']
        )

        choice(
          description: '镜像tag标记',
          name: 'ENV-TAG',
          choices: ['sit', 'uat', 'prod']
        )

        choice(
          description: '是否执行 npm install',
          name: 'NPM_INSTALL',
          choices: ['no', 'yes']
        )

        choice(
          description: '是否发送通知邮件',
          name: 'SEND_MAIL',
          choices: ['no', 'yes']
        )

        gitParameter name: 'TAG',
                     description: '如果要编译部署最新的代码，请选择origin/master',
                     type: 'PT_BRANCH_TAG',
                     defaultValue: 'origin/master'

    // string(name: 'DOCKER_IMAGE', defaultValue: 'ipm/ipm-web-uat', description: '生成的Docker image名称')
    }

    stages {
        stage('Params Init') {
            steps {
                script {
                    if (params.TAG == 'origin/master' || params.TAG == 'master') {
                        IMG_TAG = 'latest'
                    }else {
                        IMG_TAG = params.TAG
                    }
                    echo "IMG_TAG= ${IMG_TAG}"
                }
            }
        }

        stage('Git branch') {
            steps {
                script {
                    checkout([$class: 'GitSCM',
                                branches: [[name: 'master']],
                                doGenerateSubmoduleConfigurations: false,
                                gitTool: 'Default',
                                submoduleCfg: [],
                                userRemoteConfigs: [[url: 'http://10.60.22.178/ipm/ipm-web.git', credentialsId: 'suidepeng', ]]
                    ])
                    if (IMG_TAG != 'latest') {
                        checkout([$class: 'GitSCM',
                                branches: [[name: "${IMG_TAG}"]],
                                doGenerateSubmoduleConfigurations: false,
                                gitTool: 'Default',
                                submoduleCfg: [],
                                userRemoteConfigs: [[url: 'http://10.60.22.178/ipm/ipm-web.git', credentialsId: 'suidepeng', ]]
                        ])
                    }
                }
            }
        }

        stage('Docker-Compose Build') {
            when { expression { params.ONLY_DEPLOY == 'no' } }
            //  agent{label 'slave136'}
            steps {
                script {
                    sh 'cat /etc/hostname'
                    echo "部署到：${params.TARGET}"
                    echo "npm install：${params.NPM_INSTALL}"
                    echo "发送邮件：${params.SEND_MAIL}"
                    echo "GIT_TAG = ${GIT_TAG}"
                    echo "params.TAG= ${params.TAG}"
                    if (params.NPM_INSTALL == 'yes') {
                        echo 'npm install'
                        sh 'npm config set user 0'
                        sh 'npm config set unsafe-perm true'
                        sh 'npm config set strict-ssl false'
                        sh 'npm install'
                    }
                }

                sh 'node --version'
                sh 'cat /etc/hostname'

                script {
                    if (params.TARGET == 'sit') {
                        echo "集成测试环境: npm run build:sit, Docker image name = ipm/${APP_NAME}-${params.TARGET}"
                        sh 'npm run build:sit'
             }else if ( params.TARGET == 'uat') {
                        echo "验收测试环境: npm run build:uat, Docker image name = ipm/${APP_NAME}-${params.TARGET}"
                        sh 'npm run build:uat'
             }else if ( params.TARGET == 'prod') {
                        echo "生产环境: npm run build:prod, Docker image name = ipm/${APP_NAME}-${params.TARGET}"
                        sh 'npm run build:prod'
             }else {
                        echo '请指定编译部署的目标环境：集成测试 or 验收测试 or 生产'
                    }
                }

                echo 'Docker-build'
                echo  "GIT_TAG: ${GIT_TAG}"
                sh "docker login -u ${HARBOR_USER} -p ${HARBOR_PSW} ${HARBOR_HOST} "
                sh "docker build -f ./mydockerfile/Dockerfile -t ${HARBOR_HOST}/ipm/${APP_NAME}-${params.TARGET}:${IMG_TAG} . --no-cache"
                sh "docker push ${HARBOR_HOST}/ipm/${APP_NAME}-${params.TARGET}:${IMG_TAG}"
                sh "docker rmi ${HARBOR_HOST}/ipm/${APP_NAME}-${params.TARGET}:${IMG_TAG}"
            }
        }

        stage('check images') {
            when { expression { params.ONLY_DEPLOY == 'yes' } }
            steps {
                echo 'check images exist'
                sh "docker pull ${HARBOR_HOST}/ipm/${APP_NAME}-${params.TARGET}:${IMG_TAG}"
            }
        }

        //agent{label 'slave136'}
        stage('Deploy') {
            agent {
                docker {
                    image 'lwolf/helm-kubectl-docker'
                }
            //  label 'slave136'
            }

            steps {
                sh 'cat /etc/hostname'

                echo "TARGET = ${params.TARGET}"
                sh 'mkdir -p ~/.kube'
                script {
                    if (params.TARGET == 'prod') {
                        echo '#########for product'
                        sh "echo ${K8S_PROD_CONFIG} | base64 -d > ~/.kube/config"
                   }else if (params.TARGET == 'uat') {
                        echo '#########for uat'
                        sh "echo ${K8S_UAT_CONFIG} | base64 -d > ~/.kube/config"
                    }
                   else {
                        echo '#########for sit'
                        sh "echo ${K8S_SIT_CONFIG} | base64 -d > ~/.kube/config"
                   }
                }

                sh "sed -e 's#{IMAGE_URL}#${HARBOR_HOST}/ipm/${APP_NAME}-prod#g;s#{IMAGE_TAG}#${IMG_TAG}#g;s#{APP_NAME}#${APP_NAME}#g' ${params.TARGET}-deployment.tpl > ${params.TARGET}-deployment.yml"
                sh "sed -e 's#{APP_NAME}#${APP_NAME}#g' ipm-web-service.tpl > ipm-web-service.yml"
                script {
                    try {
                        sh "kubectl  delete  -f  ${params.TARGET}-deployment.yml"
                        sh 'kubectl  delete  -f  ipm-web-service.yml'
                     } finally {
                        echo '******finally*****'
                        sh "kubectl  apply  -f  ${params.TARGET}-deployment.yml"
                        sh 'kubectl  apply  -f  ipm-web-service.yml'
                    }
                }
            }
        }
    }

    post {
        success {
            echo 'success'
            script {
                if (params.SEND_MAIL == 'yes') {
                    emailext body: '''<!DOCTYPE html>
        <html>
        <head>
        <meta charset="UTF-8">
        </head><table>
        <tr>
            <td><br />
            <b><font color="#0B610B">构建信息</font></b>
            <hr size="2" width="100%" align="center" /></td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>构建名称：${JOB_NAME}</li>
                    <li>构建结果: <span style="color:green"> ${BUILD_STATUS}</span></li>
                    <li>构建编号：${BUILD_NUMBER}  </li>
                    <li>变更记录: ${CHANGES,showPaths=true,showDependencies=true,format="<pre><ul><li>提交ID: %r</li><li>提交人：%a</li><li>提交时间：%d</li><li>提交信息：%m</li><li>提交文件：<br />%p</li></ul></pre>",pathFormat="         %p <br />"}
                </ul>
            </td>
            </tr>
            </table>
            </body>
            </html>
            ''', subject: '${PROJECT_NAME}第${BUILD_NUMBER}次构建结果:${BUILD_STATUS}', to: 'wangshuang1@fawjiefang.com.cn,'
                }
            }
        }

        failure {
            echo 'failure'
            script {
                if (params.SEND_MAIL == 'yes') {
                    emailext body: '''<!DOCTYPE html>
            <html>
            <head>
            <meta charset="UTF-8">
            </head>
            <body>
            <table>
            <tr>
                <td><br />
                <b><font color="#0B610B">构建信息</font></b>
                <hr size="2" width="100%" align="center" /></td>
            </tr>
            <tr>
            <td>
                <ul>
                    <li>构建名称：${JOB_NAME}</li>
                    <li>构建结果: <span style="color:red"> ${BUILD_STATUS}</span></li>
                    <li>构建编号：${BUILD_NUMBER}  </li>
                    <li>变更记录: ${CHANGES,showPaths=true,showDependencies=true,format="<pre><ul><li>提交ID: %r</li><li>提交人：%a</li><li>提交时间：%d</li><li>提交信息：%m</li><li>提交文件：%p</li></ul></pre>",pathFormat="%p <br />"}
                </ul>
            </td>
        </tr>
        <tr>
            <td><b><font color="#0B610B">构建日志 :</font></b>
            <hr size="2" width="100%" align="center" /></td>
        </tr>
        <tr>
            <td><textarea cols="150" rows="30" readonly="readonly"
                    style="font-family: Courier New">${BUILD_LOG}</textarea>
            </td>
        </tr>
        </table>
        </body>
        </html>
        ''', subject: '${PROJECT_NAME}第${BUILD_NUMBER}次构建结果:${BUILD_STATUS}', to: 'wangshuang1@fawjiefang.com.cn,'
                }
            }
        }
    }
}
