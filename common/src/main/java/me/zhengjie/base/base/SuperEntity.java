package me.zhengjie.base.base;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 演示实体父类
 *
 * @author jffaw
 */
@Data
@Accessors(chain = true)
public class SuperEntity<T extends Model> extends Model implements Serializable {

    /**
     * 主键ID , 这里故意演示注解可以无
     */
    private Long id;
    private Long tenantId;

    @Override
    public Serializable pkVal() {
        return this.id;
    }
}
