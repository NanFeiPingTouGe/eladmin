package me.zhengjie.annotation;

import java.lang.annotation.*;

/**
 * @author jacky
 *  用于标记匿名访问
 */
@Inherited
@Documented
@Target({ElementType.METHOD,ElementType.ANNOTATION_TYPE,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnonymousAccess {

}
