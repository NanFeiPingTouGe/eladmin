package me.zhengjie.api.conf.util;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Date;

@Component
@Slf4j
@Data
public class Sequence {

    private static Long maxDataCenterId = 31L;
    private static Long maxWorkerId = 31L;
    private static Long dataCenterId;
    private static Long workerId;

    @Bean
    public Snowflake getSnowflake(){
        dataCenterId = getDatacenterId();
        workerId = getMaxWorkerId();
        log.info("当前机器的workerId:{}", workerId);
        log.info("当前机器的dataCenterId:{}", dataCenterId);
        return new Snowflake(new Date(), workerId, dataCenterId, true);
    }

    public Sequence() {
        //通过当前物理网卡地址获取datacenterId
        dataCenterId = getDatacenterId();
        //物理网卡地址+jvm进程pi获取workerId
        workerId = getMaxWorkerId();
    }

    private static long getDatacenterId() {
        long id = 0L;
        try {
            //获取本机(或者服务器ip地址)
            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            //一般不是null会进入else
            if (network == null) {
                id = 1L;
            } else {
                //获取物理网卡地址
                byte[] mac = network.getHardwareAddress();
                if (null != mac) {
                    id = ((0x000000FF & (long) mac[mac.length - 2]) | (0x0000FF00 & (((long) mac[mac.length - 1]) << 8))) >> 6;
                    id = id % (maxDataCenterId + 1);
                }
            }
        } catch (Exception e) {
            log.warn(" getDatacenterId: " + e.getMessage());
        }
        return id;
    }

    /**
     * 获取 maxWorkerId
     */
    private static long getMaxWorkerId() {
        StringBuilder mpid = new StringBuilder();
        mpid.append(dataCenterId);
        //获取jvm进程信息
        String name = ManagementFactory.getRuntimeMXBean().getName();
        if (StrUtil.isNotBlank(name)) {
            /*
             * 获取进程PID
             */
            mpid.append(name.split("@")[0]);
        }
        /*
         * MAC + PID 的 hashcode 获取16个低位
         */
        return (mpid.toString().hashCode() & 0xffff) % (maxWorkerId + 1);
    }


    /**
     * 有参构造器
     *
     * @param workerId     工作机器 ID
     * @param dataCenterId 序列号
     */
    public Sequence(long workerId, long dataCenterId) {
        Assert.isFalse(workerId > maxWorkerId || workerId < 0,
                String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        Assert.isFalse(dataCenterId > maxDataCenterId || dataCenterId < 0,
                String.format("datacenter Id can't be greater than %d or less than 0", maxDataCenterId));
        this.workerId = workerId;
        this.dataCenterId = dataCenterId;
    }


}

