package me.zhengjie.api.conf;

import lombok.extern.slf4j.Slf4j;
import me.zhengjie.utils.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    Result<Object> handleException(Exception e) {
        log.info(e.getMessage());
        e.printStackTrace();
        return Result.failure(e.getMessage());
    }
}
