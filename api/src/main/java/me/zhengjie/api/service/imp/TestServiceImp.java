package me.zhengjie.api.service.imp;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.zhengjie.api.entity.Test;
import me.zhengjie.api.mapper.TestMapper;
import me.zhengjie.api.service.TestService;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.lang.reflect.Method;

@Service
@Slf4j
@RequiredArgsConstructor
public class TestServiceImp extends ServiceImpl<TestMapper, Test> implements TestService {

    private final TestMapper testMapper;
    private final ApplicationContext applicationContext;
    private final SqlSessionTemplate sqlSessionTemplate;
    private final SqlSession sqlSession;

    @Override
    public Page<Test> findAll(String t) {
        // 不进行 count sql 优化，解决 MP 无法自动优化 SQL 问题，这时候你需要自己查询 count 部分
        // page.setOptimizeCountSql(false);
        // 当 total 为小于 0 或者设置 setSearchCount(false) 分页插件不会进行 count 查询
        // 要点!! 分页返回的对象与传入的对象是同一个
        return testMapper.selectPageList(new Page().setSize(50), t);
    }

    @PostConstruct
    public void test(){
        Class<?> testMapper1 = applicationContext.getType("testMapper");
        final Method[] declaredMethods = testMapper1.getDeclaredMethods();
        cn.hutool.core.lang.Console.log(this.testMapper.toString());
    }
}
