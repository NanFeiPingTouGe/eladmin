package me.zhengjie.api.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import me.zhengjie.api.entity.Test;
import org.springframework.stereotype.Service;

public interface TestService extends IService<Test> {
    Page<Test> findAll(String t);
}
