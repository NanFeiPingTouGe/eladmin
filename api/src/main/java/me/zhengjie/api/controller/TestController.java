package me.zhengjie.api.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.api.entity.Test;
import me.zhengjie.api.mapper.TestMapper;
import me.zhengjie.api.service.TestService;
import me.zhengjie.utils.Result;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Api(tags = "测试")
@RequiredArgsConstructor
@RequestMapping("/test")
@Slf4j
@AnonymousAccess
//@SaCheckLogin
public class TestController {

    private final TestService testService;
    private final SqlSessionTemplate sqlSessionTemplate;

    @RequestMapping("/findAll")
    public Result<Page<Test>> findAll(@RequestParam String t) {
        sqlSessionTemplate.selectList("selectPageList");
        return Result.success(testService.findAll(t));
    }

    // 测试登录，浏览器访问： http://localhost:8081/user/doLogin?username=zhang&password=123456
    @GetMapping("doLogin")
    public Result doLogin(String username, String password) {
        // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对
        if("zhang".equals(username) && "123456".equals(password)) {
            StpUtil.login(10001,"PC");
            return Result.success(StpUtil.getTokenInfo());
        }
        return Result.failure("登陆失败");
    }

    // 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin
    @RequestMapping("isLogin")
    public String isLogin() {
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

    public static String say() {
        return "say";
    }

    public static void main(String[] args) {
        System.out.println(IdWorker.getId());
    }

}
