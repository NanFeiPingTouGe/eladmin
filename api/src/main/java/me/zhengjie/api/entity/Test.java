package me.zhengjie.api.entity;

import lombok.Data;
import me.zhengjie.base.base.SuperEntity;

import java.io.Serializable;

/**
 * @author jffaw
 */
@Data
public class Test implements Serializable {

    private String t;
}
