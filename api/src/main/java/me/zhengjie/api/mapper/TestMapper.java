package me.zhengjie.api.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import me.zhengjie.api.entity.Test;
import me.zhengjie.base.base.SuperMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TestMapper extends SuperMapper<Test> {

    //    @Select("select * from test")
    Page<Test> selectPageList(Page<?> page, String t);

}
